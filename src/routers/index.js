const { successResponse } = require('../utils/helpers');
const productRouteTable = require('./Product');
const orderRouterTable = require('./Order');
const smartHomeRouterTable = require('./SmartHome');

module.exports = [
    { method: 'get', path: '', handler: (_, resp) => successResponse(resp, `Enjoy Explore-Word APIs!` ) },
    ...productRouteTable,
    ...orderRouterTable,
    ...smartHomeRouterTable,
];
