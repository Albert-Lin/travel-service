const SmartHomeController = require('../controllers/SmartHome');

module.exports = [
    { method: 'post', path: 'smarthome/reset', handler: SmartHomeController.reset },
    // response: {}
    { method: 'put', path: 'smarthome/close', handler: SmartHomeController.close },
    // response: {}
    { method: 'get', path: 'smarthome/rooms', handler: SmartHomeController.rooms },
    // response:
    // [
    //     {
    //         id: 'r00',
    //         name: 'Living Room',
    //         devices: [
    //             'd00', 'd01', 'd02', 'd03', 'd04',
    //             'd05', 'd06', 'd07', 'd08', 'd09',
    //             'd10', 'd11', 'd12', 'd13', 'd14',
    //             'd15', 'd16', 'd17', 'd18', 'd19',
    //             'd20', 'd21', 'd22',
    //         ],
    //     },
    //     ...
    // ]
    { method: 'post', path: 'smarthome/room', handler: SmartHomeController.addRoom },
    // response:
    // [
    //     {
    //         id: 'r00',
    //         name: 'Living Room',
    //         devices: [
    //             'd00', 'd01', 'd02', 'd03', 'd04',
    //             'd05', 'd06', 'd07', 'd08', 'd09',
    //             'd10', 'd11', 'd12', 'd13', 'd14',
    //             'd15', 'd16', 'd17', 'd18', 'd19',
    //             'd20', 'd21', 'd22',
    //         ],
    //     },
    //     ...
    // ]
    { method: 'delete', path: 'smarthome/room/:roomID', handler: SmartHomeController.removeRoom },
    // response:
    // [
    //     {
    //         id: 'r00',
    //         name: 'Living Room',
    //         devices: [
    //             'd00', 'd01', 'd02', 'd03', 'd04',
    //             'd05', 'd06', 'd07', 'd08', 'd09',
    //             'd10', 'd11', 'd12', 'd13', 'd14',
    //             'd15', 'd16', 'd17', 'd18', 'd19',
    //             'd20', 'd21', 'd22',
    //         ],
    //     },
    //     ...
    // ]
    { method: 'get', path: 'smarthome/all-active-devices-count', handler: SmartHomeController.allActiveDeviceCount},
    // response
    // {
    //     all: 11,
    //     except: 10
    // }
    { method: 'get', path: 'smarthome/:roomID/devices', handler: SmartHomeController.devices },
    // response:
    // [
    //     {
    //         devices: [
    //             {
    //                 id: 'd00',
    //                 type: 'router',
    //                 name: 'Router',
    //                 roomID: 'r00',
    //                 totalUsing: 800,
    //                 using: 1440,
    //                 status: true,
    //                 setting: { ... },
    //             },
    //             ...
    //         ],
    //         activeDevicesCount: 10,
    //     }
    // ]
    { method: 'get', path: 'smarthome/device/:deviceID', handler: SmartHomeController.device },
    // response:
    // {
    //     id: 'd00',
    //     type: 'router',
    //     name: 'Router',
    //     roomID: 'r00',
    //     totalUsing: 800,
    //     using: 1440,
    //     status: true,
    //     setting: { ... },
    // }
    { method: 'put', path: 'smarthome/device/setting', handler: SmartHomeController.updateDeviceSetting },
    // response: {}
    { method: 'put', path: 'smarthome/home/turn-on-all-devices', handler: SmartHomeController.turnOnAllRoomsDevices },
    // response:
    { method: 'put', path: 'smarthome/:roomID/turn-on-all-devices', handler: SmartHomeController.turnOnRoomDevices },
    // response:
    // [
    //     {
    //         id: 'd00',
    //         type: 'router',
    //         name: 'Router',
    //         roomID: 'r00',
    //         totalUsing: 800,
    //         using: 1440,
    //         status: true,
    //         setting: { ... },
    //     },
    //     ...
    // ]
    { method: 'put', path: 'smarthome/turn-on/:deviceID', handler: SmartHomeController.turnOnCustomDevice },
    // response:
    // {
    //     id: 'd00',
    //     type: 'router',
    //     name: 'Router',
    //     roomID: 'r00',
    //     totalUsing: 800,
    //     using: 1440,
    //     status: true,
    //     setting: { ... },
    // }
    { method: 'put', path: 'smarthome/home/turn-off-all-devices', handler: SmartHomeController.turnOffAllRoomsDevices },
    // response:
    { method: 'put', path: 'smarthome/:roomID/turn-off-all-devices', handler: SmartHomeController.turnOffRoomDevices },
    // response:
    // [
    //     {
    //         id: 'd00',
    //         type: 'router',
    //         name: 'Router',
    //         roomID: 'r00',
    //         totalUsing: 800,
    //         using: 1440,
    //         status: true,
    //         setting: { ... },
    //     },
    //     ...
    // ]
    { method: 'put', path: 'smarthome/turn-off/:deviceID', handler: SmartHomeController.turnOffCustomDevice },
    // response:
    // {
    //     id: 'd00',
    //     type: 'router',
    //     name: 'Router',
    //     roomID: 'r00',
    //     totalUsing: 800,
    //     using: 1440,
    //     status: true,
    //     setting: { ... },
    // }
    { method: 'post', path: 'smarthome/:roomID/device', handler: SmartHomeController.addDevice },
    // response:
    // [
    //     {
    //         id: 'd00',
    //         type: 'router',
    //         name: 'Router',
    //         roomID: 'r00',
    //         totalUsing: 800,
    //         using: 1440,
    //         status: true,
    //         setting: { ... },
    //     }
    // ]
    { method: 'delete', path: 'smarthome/device/:deviceID', handler: SmartHomeController.removeDevice },
    // response:
    // [
    //     {
    //         id: 'd00',
    //         type: 'router',
    //         name: 'Router',
    //         roomID: 'r00',
    //         totalUsing: 800,
    //         using: 1440,
    //         status: true,
    //         setting: { ... },
    //     }
    // ]
    { method: 'put', path: 'smarthome/:deviceID/music-player/:action', handler: SmartHomeController.actionOnMusicDevice },
    // response:
    // {
    //     id: 'd01',
    //     type: 'music',
    //     name: 'Music',
    //     roomID: 'r00',
    //     totalUsing: 80,
    //     using: 40,
    //     status: true,
    //     setting: {
    //         cover: './asset/image/device/device-02.avif',
    //         name: 'Inception',
    //         composer: 'Hans Zimmer',
    //         default: {
    //             range: {
    //                 min: 0,
    //                 max: 280,
    //             },
    //             value: 12,
    //             status: false,
    //             timer: undefined,
    //             type: 'repeat',
    //         },
    //     },
    // }
    { method: 'put', path: 'smarthome/:deviceID/music-setting/:playingType', handler: SmartHomeController.actionOnMusicDevice },
    // response:
    // {
    //     id: 'd01',
    //     type: 'music',
    //     name: 'Music',
    //     roomID: 'r00',
    //     totalUsing: 80,
    //     using: 40,
    //     status: true,
    //     setting: {
    //         cover: './asset/image/device/device-02.avif',
    //         name: 'Inception',
    //         composer: 'Hans Zimmer',
    //         default: {
    //             range: {
    //                 min: 0,
    //                 max: 280,
    //             },
    //             value: 12,
    //             status: false,
    //             timer: undefined,
    //             type: 'repeat',
    //         },
    //     },
    // }
    { method: 'put', path: 'smarthome/:deviceID/music-play-at/:time', handler: SmartHomeController.actionOnMusicDevice },
    // response:
    // {
    //     id: 'd01',
    //     type: 'music',
    //     name: 'Music',
    //     roomID: 'r00',
    //     totalUsing: 80,
    //     using: 40,
    //     status: true,
    //     setting: {
    //         cover: './asset/image/device/device-02.avif',
    //         name: 'Inception',
    //         composer: 'Hans Zimmer',
    //         default: {
    //             range: {
    //                 min: 0,
    //                 max: 280,
    //             },
    //             value: 12,
    //             status: false,
    //             timer: undefined,
    //             type: 'repeat',
    //         },
    //     },
    // }
    { method: 'put', path: 'smarthome/:deviceID/music-volume-setting/:volume', handler: SmartHomeController.updateMusicVolume },
    // request: volume (數字)
    // response:
    // {
    //     id: 'd01',
    //     type: 'music',
    //     name: 'Music',
    //     roomID: 'r00',
    //     totalUsing: 80,
    //     using: 40,
    //     status: true,
    //     setting: {
    //         cover: './asset/image/device/device-02.avif',
    //         name: 'Inception',
    //         composer: 'Hans Zimmer',
    //         default: {
    //             range: {
    //                 min: 0,
    //                 max: 280,
    //             },
    //             value: 12,
    //             status: false,
    //             timer: undefined,
    //             type: 'repeat',
    //         },
    //     },
    // }
    { method: 'get', path: 'smarthome/all-rooms/power-consumption', handler: SmartHomeController.powerTotalUsageForAllRooms},
    // response:
    // {
    //     sum: 33,
    //     name: [ 'room-0', 'room-1', 'room-2' ],
    //     totalUsingList: [10, 11, 12],
    // }
    { method: 'get', path: 'smarthome/:roomID/power-consumption', handler: SmartHomeController.powerTotalUsageForSingleRoom},
    // response:
    // {
    //     sum: 33,
    //     name: [ 'device-0', 'device-1', 'device-2' ],
    //     totalUsingList: [10, 11, 12],
    // }
    { method: 'get', path: 'smarthome/home/line-chart', handler: SmartHomeController.lineStatistic},
    // response:
    // {
    //     labels: [ '', '', '' ],
    //     values: [
    //         [ 100, 200, 300 ],
    //         [ 400, 500, 600 ],
    //     ]
    // }
    { method: 'get', path: 'smarthome/:roomID/line-chart', handler: SmartHomeController.lineStatistic},
    // response:
    // {
    //     labels: [ '', '', '' ],
    //     values: [
    //         [ 100, 200, 300 ],
    //     ]
    // }
];
