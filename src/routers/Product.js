const ProductController = require('../controllers/Product');

// Product Example:
const Full_PRODUCT_EXAMPLE = {
    id: 'c41',
    continent: 'antarctica',
    imgSetting: [
        {
            src: './asset/image/card/antarctica/antarctica-1.avif',
            style: '',
        },
        {
            src: './asset/image/card/antarctica/antarctica-2.avif',
            style: '',
        },
        {
            src: './asset/image/card/antarctica/antarctica-3.avif',
            style: '',
        },
        {
            src: './asset/image/card/antarctica/antarctica-4.avif',
            style: 'filter: brightness(120%);',
        },
    ],
    country: `Antarctica`,
    location: `Antarctica`,
    describe: `Antarctica is Earth's southernmost continent. About 70% of the world's freshwater reserves are frozen in Antarctica, which if melted would raise global sea levels by almost 60 meters. Antarctica holds the record for the lowest measured temperature on Earth. Native species of animals include mites, nematodes, penguins, seals and tardigrades. Where vegetation occurs, it is mostly in the form of lichen or moss.`,
    rate: '5.0',
    price: 20000,
    isFavorite: false,
    reviews: {
        transportation: '5.0',
        hotel: '5.0',
        food: '5.0',
        service: '5.0',
    },
    map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23243278.44060305!2d0!3d-69.17165706722953!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xa4b9967b3390754b%3A0x6e52be1f740f2075!2sAntarctica!5e0!3m2!1sen!2stw!4v1653874994595!5m2!1sen!2stw`,
    tag: [ 'nature', 'animal', '南極', '自然', '動物', ],
};
const SIMPLE_PRODUCT_EXAMPLE = {
    id: 'c41',
    continent: 'antarctica',
    imgSetting: [
        {
            src: './asset/image/card/antarctica/antarctica-1.avif',
            style: '',
        },
        {
            src: './asset/image/card/antarctica/antarctica-2.avif',
            style: '',
        },
        {
            src: './asset/image/card/antarctica/antarctica-3.avif',
            style: '',
        },
        {
            src: './asset/image/card/antarctica/antarctica-4.avif',
            style: 'filter: brightness(120%);',
        },
    ],
    country: `Antarctica`,
    location: `Antarctica`,
    describe: `Antarctica is Earth's southernmost continent. About 70% of the world's freshwater reserves are frozen in Antarctica, which if melted would raise global sea levels by almost 60 meters. Antarctica holds the record for the lowest measured temperature on Earth. Native species of animals include mites, nematodes, penguins, seals and tardigrades. Where vegetation occurs, it is mostly in the form of lichen or moss.`,
    rate: '5.0',
    price: 20000,
    isFavorite: false,
};

module.exports = [
    { method: 'put', path: 'travel/card/init', handler: ProductController.init },
    // request:
    // response:

    { method: 'get', path: 'travel/card/:cardID', handler: ProductController.product },
    // request (required): :cardID
    // request (optional): w=number, q=number
    // response: Full_PRODUCT_EXAMPLE

    { method: 'get', path: 'travel/cards/:type', handler: ProductController.products },
    // request (required): :type (all, continent, keywords)
    // request (optional): w=number, q=number, full=true, value=string, imgCount=number, sort=string, sequence=asc|desc, favorite=boolean
    // response:
    // [
    //     Full_PRODUCT_EXAMPLE or SIMPLE_PRODUCT_EXAMPLE
    // ]

    { method: 'put', path: 'travel/card/:cardID/:action/favorite', handler: ProductController.updateFavorite },
    // request: :cardID, :action (add, remove)
    // response:

    { method: 'get', path: 'travel/card/ticket/:cardID', handler: ProductController.flightOfProduct },
    // request (required): :cardID
    // response:
    // {
    //     id: 't41',
    //     cardID: 'c41',
    //     departure: {
    //         time: `23:30`,
    //         place: `Taoyuan`,
    //         airport: `TPE`,
    //     },
    //     arrival: {
    //         time: `08:05`,
    //         place: `Ushuaia`,
    //         airport: `USH`,
    //     },
    //     elapsedTime: `43h35m`,
    // }

    { method: 'get', path: 'travel/ticket/:ticketID', handler: ProductController.flight },
    // request (required): :ticketID
    // response:
    // {
    //     id: 't41',
    //     cardID: 'c41',
    //     departure: {
    //         time: `23:30`,
    //         place: `Taoyuan`,
    //         airport: `TPE`,
    //     },
    //     arrival: {
    //         time: `08:05`,
    //         place: `Ushuaia`,
    //         airport: `USH`,
    //     },
    //     elapsedTime: `43h35m`,
    // }

    { method: 'get', path: 'travel/:ticketID/:timestamp/seats', handler: ProductController.availableFlightSeat },
    // request (required): :ticketID, :timestamp
    // response:
    // {
    //     departure: {
    //         '1A': false,
    //         '1B': false,
    //         '1C': true,
    //         // ...
    //     },
    //     arrival: {
    //         '1A': true,
    //         '1B': false,
    //         '1C': false,
    //         // ...
    //     }
    // }
];
