const _smartHomeService = require('../services/SmartHome');
const { successResponse, requestIP } = require('../utils/helpers');

function _userKey(request) {
    const ip = requestIP(request);
    const loginID = request.header('auth');
    return _smartHomeService.userKey(ip, loginID);
}

module.exports = {
    // for all
    reset(req, resp) {
        const loginID = _smartHomeService.initUserData(requestIP(req));
        successResponse(resp, { loginID });
    },
    close(req, resp) {
        _smartHomeService.terminate(_userKey(req));
        successResponse(resp, {});
    },
    // room
    rooms(req, resp) {
        const rooms = _smartHomeService.rooms(_userKey(req), []);
        successResponse(resp, rooms);
    },
    addRoom(req, resp) {
        const roomSetting = req.body;
        _smartHomeService.addRoom(_userKey(req), roomSetting);
        const rooms = _smartHomeService.rooms(_userKey(req), []);
        successResponse(resp, rooms);
    },
    removeRoom(req, resp) {
        const roomID = req.params['roomID'];
        _smartHomeService.removeRoom(_userKey(req), roomID);
        const rooms = _smartHomeService.rooms(_userKey(req), []);
        successResponse(resp, rooms);
    },
    turnOnAllRoomsDevices(req, resp) {
        const userKey = _userKey(req);
        const allRooms = _smartHomeService.rooms(userKey);
        const allDevices = allRooms
            .map(({ id: roomID }) => _smartHomeService.room(userKey, roomID).devices)
            .flat();
        _smartHomeService.turnOnDevices(userKey, allDevices);
        successResponse(resp);
    },
    turnOffAllRoomsDevices(req, resp) {
        const userKey = _userKey(req);
        const allRooms = _smartHomeService.rooms(userKey);
        const allDevices = allRooms
            .map(({ id: roomID }) => _smartHomeService.room(userKey, roomID).devices)
            .flat();
        _smartHomeService.turnOffDevices(userKey, allDevices, true);
        successResponse(resp);
    },
    powerTotalUsageForAllRooms(req, resp) {
        const userKey = _userKey(req);
        const rooms = _smartHomeService.rooms(userKey, []);
        let sum = 0;
        const names = [];
        const totalUsingList = rooms
            .map(({ id, name }) => {
                const singleRoomResult = _smartHomeService.singleRoomPowerTotalUsage(userKey, id);
                sum += singleRoomResult.sum;
                names.push(name);
                return singleRoomResult.sum;
            });
        successResponse(resp, { names, totalUsingList, sum });
    },
    // device
    allActiveDeviceCount(req, resp) {
        const rooms = _smartHomeService.rooms(_userKey(req), undefined);
        const activeDevices = rooms
            .map(({ devices }) => devices)
            .flat()
            .map((deviceID) => {
                return _smartHomeService.device(_userKey(req), deviceID);
            })
            .filter(({ status }) => !!status);

        const activeDeviceExceptUnableAutoTurnOff = activeDevices
            .filter(({ type }) => !_smartHomeService.DEVICE_TYPES_OF_UNABLE_AUTO_TURN_OFF.includes(type));

        successResponse(resp, {
            all: activeDevices.length,
            except: activeDeviceExceptUnableAutoTurnOff.length
        });
    },
    device(req, resp) {
        const deviceID = req.params['deviceID'];
        const device = _smartHomeService.device(_userKey(req), deviceID);
        successResponse(resp, device);
    },
    updateDeviceSetting(req, resp) {
        const deviceData = req.body;
        _smartHomeService.updateDeviceSetting(_userKey(req), deviceData);
        successResponse(resp);
    },
    devices(req, resp) {
        const roomID = req.params['roomID'];
        const devices = _smartHomeService.devices(_userKey(req), roomID);
        const activeDevicesCount = _smartHomeService.activeDevices(_userKey(req), devices).length;
        successResponse(resp, {
            devices,
            activeDevicesCount,
        });
    },
    turnOnRoomDevices(req, resp) {
        const roomID = req.params['roomID'];
        const devicesIDList = _smartHomeService.room(_userKey(req), roomID).devices;
        _smartHomeService.turnOnDevices(_userKey(req), devicesIDList);
        const devices = _smartHomeService.devices(_userKey(req), roomID);
        successResponse(resp, devices);
    },
    turnOnCustomDevice(req, resp) {
        const deviceID = req.params['deviceID'];
        _smartHomeService.turnOnDevices(_userKey(req), [ deviceID ]);
        const device = _smartHomeService.device(_userKey(req), deviceID);
        successResponse(resp, device);
    },
    turnOffRoomDevices(req, resp) {
        const roomID = req.params['roomID'];
        const devicesIDList = _smartHomeService.room(_userKey(req), roomID).devices;
        _smartHomeService.turnOffDevices(_userKey(req), devicesIDList, true);
        const devices = _smartHomeService.devices(_userKey(req), roomID);
        successResponse(resp, devices);
    },
    turnOffCustomDevice(req, resp) {
        const deviceID = req.params['deviceID'];
        _smartHomeService.turnOffDevices(_userKey(req), [ deviceID ], false);
        const device = _smartHomeService.device(_userKey(req), deviceID);
        successResponse(resp, device);
    },
    addDevice(req, resp) {
        const roomID = req.params['roomID'];
        const deviceData = { ...req.body, roomID };
        _smartHomeService.addDevice(_userKey(req), deviceData);
        const devices = _smartHomeService.devices(_userKey(req), roomID);
        successResponse(resp, devices);
    },
    removeDevice(req, resp) {
        const deviceID = req.params['deviceID'];
        const roomIDOfDevice = _smartHomeService.device(_userKey(req), deviceID).roomID;
        _smartHomeService.removeDevice(_userKey(req), deviceID);
        const devices = _smartHomeService.devices(_userKey(req), roomIDOfDevice);
        successResponse(resp, devices);
    },
    powerTotalUsageForSingleRoom(req, resp) {
        const roomID = req.params['roomID'];
        const result = _smartHomeService.singleRoomPowerTotalUsage(_userKey(req), roomID);
        successResponse(resp, result);
    },
    // music
    actionOnMusicDevice(req, resp) {
        const deviceID = req.params['deviceID'];
        const action = req.params['action'];
        const playingType = req.params['playingType'];
        const playAt = req.params['time'];
        if (action === 'play') {
            _smartHomeService.playMusic(_userKey(req), deviceID);
        }
        if (action === 'pause') {
            _smartHomeService.stopMusic(_userKey(req), deviceID, 'pause');
        }
        if (action === 'stop') {
            _smartHomeService.stopMusic(_userKey(req), deviceID, 'stop');
        }
        if (playingType) {
            const setting = {
                keyPath: 'default.type',
                value: playingType,
            };
            _smartHomeService.updateMusic(_userKey(req), deviceID, setting);
        }
        if (playAt) {
            const setting = {
                keyPath: 'default.value',
                value: Number(playAt),
            };
            _smartHomeService.updateMusic(_userKey(req), deviceID, setting);
        }
        const device = _smartHomeService.device(_userKey(req), deviceID);
        successResponse(resp, device);
    },
    updateMusicVolume(req, resp) {
        const userKey = _userKey(req);
        const deviceID = req.params['deviceID'];
        const volume = Number(req.params['volume']);
        if (!isNaN(volume)) {
            const setting = {
                keyPath: 'default.volume.value',
                value: volume,
            };
            _smartHomeService.updateMusic(_userKey(req), deviceID, setting);
        }
        const device = _smartHomeService.device(userKey, deviceID);
        successResponse(resp, device);
    },
    // statistic
    lineStatistic(req, resp) {
        const roomID = req.params['roomID'];
        if (roomID) {
            const roomDailyUsing = {
                labels: [
                    '00', '02', '04', '06', '08', '10',
                    '12', '14', '16', '18', '20', '22',
                ],
                values: _smartHomeService.room(_userKey(req), roomID).dailyHistory,
            };
            successResponse(resp, roomDailyUsing);
        } else {
            const homeMonthlyUsing = {
                labels: [
                    'Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.',
                    'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.',
                ],
                values: [
                    [ 400, 280, 320, 300, 310, 360, 520, 480, 400, 380, 340, 300 ],
                    [ 760, 684, 772, 801, 823, 858, 956, 980, 998, 983, 937, 804 ],
                ],
            };
            successResponse(resp, homeMonthlyUsing);
        }
    },
};
