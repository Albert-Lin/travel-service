const { successResponse, requestIP, randomIndex } = require('../utils/helpers');
const _travelProductService = require('../services/Product');
const _travelFlightService = require('../services/Flight');

function _imageSetting(req) {
    const width = req.query['w'];
    const quality = req.query['q'];
    return [ width, quality ];
}

function _productDetailETL(product, imgSetting, imgCount, withFullDetail) {
    if (!withFullDetail) {
        delete product.reviews;
        delete product.map;
        delete product.tag;
    }
    product.imgSetting = product.imgSetting.filter((_, index) => index < imgCount);
    _travelProductService.resetImage(product, ...imgSetting);
    return product;
}

module.exports = {
    init(req, resp) {
        _travelProductService.init(requestIP(req));
        _travelFlightService.init(requestIP(req));
        successResponse(resp, {});
    },
    products(req, resp) {
        const imgSetting = _imageSetting(req);
        const imgCount = req.query['imgCount'] || 1;
        const withFullDetail = req.query['full'] === 'true';
        const type = req.params['type'].toLowerCase();
        const typeValue = req.query['value'];
        const sort = req.query['sort'];
        const sequence = req.query['sequence'];
        const favorite = req.query['favorite'] === 'true';

        // search and map
        let result = [];
        const products = _travelProductService
            .products(requestIP(req))
            .filter((product) => {
                if (type === 'all') {
                    return true;
                }

                if (type === 'continent' && !!typeValue) {
                    return product.continent === typeValue;
                }

                if (type === 'keywords' && !!typeValue) {
                    let match = false;
                    typeValue
                        .split(' ')
                        .some((keyword) => {
                            if (product.continent.toLowerCase().includes(keyword.toLowerCase())) {
                                match = true;
                                return true;
                            }
                            if (product.country.toLowerCase().split(' - ').includes(keyword.toLowerCase())) {
                                match = true;
                                return true;
                            }
                            if (product.location.toLowerCase().split(', ').includes(keyword.toLowerCase())) {
                                match = true;
                                return true;
                            }
                            const describeWords = product.describe.toLowerCase().replace(/[,.]/g, '').split(' ');
                            if (describeWords.includes(keyword.toLowerCase())) {
                                match = true;
                                return true;
                            }
                            product.tag
                                .map((t) => t.toLowerCase())
                                .some((tag) => {
                                    if (tag.includes(keyword.toLowerCase())) {
                                        match = true;
                                        return true;
                                    }
                                });
                        });
                    return match;
                }

                return false;
            })
            .map((product) => _productDetailETL(product, imgSetting, imgCount, withFullDetail))

        // random if necessary
        const toRandom = type === 'all';
        if (toRandom) {
            products
                .forEach((product) => {
                    const index = randomIndex(products.length, (num) => !result[num]);
                    result[index] = product;
                });
        }
        if (!toRandom) {
            result = products;
        }

        if (sort) {
            result = result.sort((a, b) => {
                if (sequence && sort.toLowerCase() === 'price') {
                    return sequence.toLowerCase() === 'asc'
                        ? a.price - b.price
                        : b.price - a.price;
                }

                if (sequence && sort.toLowerCase() === 'rate') {
                    return sequence.toLowerCase() === 'asc'
                        ? Number(a.rate) - Number(b.rate)
                        : Number(b.rate) - Number(a.rate);
                }

                return sort.toLowerCase() === 'price'
                    ? b.price - a.price
                    : Number(b.rate) - Number(a.rate);
            });
        }

        if (favorite) {
            result = result.filter(({ isFavorite }) => isFavorite);
        }

        successResponse(resp, result);
    },
    product(req, resp) {
        const imageSetting = _imageSetting(req);
        const productID = req.params['cardID'];
        const product = _travelProductService.product(requestIP(req), productID);
        _travelProductService.resetImage(product, ...imageSetting);
        successResponse(resp, product);
    },
    updateFavorite(req, resp) {
        const productID = req.params['cardID'];
        const action = req.params['action'];
        _travelProductService.updateFavorite(requestIP(req), productID, action);
        successResponse(resp, {});
    },
    flightOfProduct(req, resp) {
        const productID = req.params['cardID'];
        const flight = _travelFlightService.flightByProductID(productID);
        successResponse(resp, flight);
    },
    flight(req, resp) {
        const flightID = req.params['ticketID'];
        const flight = _travelFlightService.flightByID(flightID);
        successResponse(resp, flight);
    },
    availableFlightSeat(req, resp) {
        const searchConditions = {
            id: req.params['ticketID'],
            timestamp: req.params['timestamp'],
        };
        const seats = _travelFlightService.seats(requestIP(req), searchConditions);
        successResponse(resp, seats);
    }
};
