const { successResponse, errorResponse, requestIP } = require('../utils/helpers');
const _travelOrderService = require(`../services/Order`);
const _travelProductService = require(`../services/Product`);
const _travelFlightService = require(`../services/Flight`);

function _imageSetting(req) {
    const width = req.query['w'];
    const quality = req.query['q'];
    return [ width, quality ];
}

function _orderDetailETL(ip, order, imgSetting, imgCount, withFullDetail) {
    // access
    let product = _travelProductService.product(ip, (order.productID || order.cardID));
    const flight = _travelFlightService.flightByID((order.flightID || order.ticketID));
    if (!withFullDetail) {
        product = {
            imgSetting: [ product.imgSetting[0] ],
            country: product.country,
            location: product.location,
            price: product.price,
        };
    }
    product.imgSetting = product.imgSetting.filter((_, index) => index < imgCount);
    _travelProductService.resetImage(product, ...imgSetting);
    // ETL
    order.cardID = (order.productID || order.cardID);
    order.ticketID = (order.flightID || order.ticketID);
    delete order.productID;
    delete order.flightID;
    const result = { order, card: product };
    if (withFullDetail) {
        result.ticket = flight;
    }
    // result
    return result;
}

function _orderFlightSeatsValidation(ip, originOrder, newOrder) {
    const departureData = {
        id: newOrder.ticketID || newOrder.flightID,
        timestamp: newOrder.departure.timestamp,
        type: 'departure',
    };
    const arrivalData = {
        id: newOrder.ticketID || newOrder.flightID,
        timestamp: newOrder.arrival.timestamp,
        type: 'arrival',
    };
    const notAvailableDepartureSeat = newOrder.departure.seats.find((seat) => {
        const sameDate = originOrder.departure.timestamp === departureData.timestamp;
        const sameSeat = originOrder.departure.seats.includes(seat);
        return sameDate && sameSeat
            ? false
            : !_travelFlightService.isAvailableSeat(ip, { ...departureData, seat });
    });
    if (notAvailableDepartureSeat) {
        throw Error(`Departure seat (${notAvailableDepartureSeat}) already been ordered`);
    }
    const notAvailableArrivalSeat = newOrder.arrival.seats.find((seat) => {
        const sameDate = originOrder.arrival.timestamp === arrivalData.timestamp;
        const sameSeat = originOrder.arrival.seats.includes(seat);
        return sameDate && sameSeat
            ? false
            : !_travelFlightService.isAvailableSeat(ip, { ...arrivalData, seat });
    });
    if (notAvailableArrivalSeat) {
        throw Error(`Arrival seat (${notAvailableDepartureSeat}) already been ordered`);
    }
}

function _orderFlightSeatsUpdate(ip, order, toAvailable) {
    const departureData = {
        id: order.ticketID || order.flightID,
        timestamp: order.departure.timestamp,
        type: 'departure',
    };
    const arrivalData = {
        id: order.ticketID || order.flightID,
        timestamp: order.arrival.timestamp,
        type: 'arrival',
    };
    order.departure.seats.forEach((seat) => {
        _travelFlightService.updateSeat(ip, { ...departureData, seat }, toAvailable);
    });
    order.arrival.seats.forEach((seat) => {
        _travelFlightService.updateSeat(ip, { ...arrivalData, seat }, toAvailable);
    });
}

function _orderInCart(req) {
    const ip = requestIP(req);
    const imgCount = req.query['imgCount'] || 1;
    const imgSetting = _imageSetting(req);
    const withFullDetail = req.query['full'] === 'true';
    return _travelOrderService
        .ordersOf(ip, false)
        .map((order) => _orderDetailETL(ip, order, imgSetting, imgCount, withFullDetail));
}

module.exports = {
    init(req, resp) {
        _travelOrderService.init(requestIP(req));
        successResponse(resp, {});
    },
    order(req, resp) {
        const ip = requestIP(req);
        const orderID = req.params['orderID'];
        const imgSetting = _imageSetting(req);
        const imgCount = req.query['imgCount'] || 1;
        const withFullDetail = req.query['full'] === 'true';
        const order = _travelOrderService.order(ip, orderID);
        const result = _orderDetailETL(ip, order, imgSetting, imgCount, withFullDetail);
        successResponse(resp, result);
    },
    ordersInCart(req, resp) {
        const orderDetails = _orderInCart(req);
        successResponse(resp, orderDetails);
    },
    ordersOfBatch(req, resp) {
        try {
            const ip = requestIP(req);
            const batchID = req.params['batchID'];
            const imgSetting = _imageSetting(req);
            const imgCount = req.query['imgCount'] || 1;
            const withFullDetail = req.query['full'] === 'true';
            const ordersOfBatch = _travelOrderService.batchOrders(ip, batchID);
            const orderDetails = _travelOrderService
                .ordersOf(ip, true)
                .filter(({ id }) => ordersOfBatch.orders.find((order) => order.id === id))
                .map((order) => _orderDetailETL(ip, order, imgSetting, imgCount, withFullDetail));
            let payment = ordersOfBatch
                .orders
                .map(({ price }) => price)
                .reduce((sum, current) => sum + current, 0);
            if (ordersOfBatch.useDiscount) {
                payment *= 0.95;
            }
            successResponse(resp, {
                orders: orderDetails,
                payment,
            });
        } catch (e) {
            errorResponse(resp, e, 404);
        }
    },
    addOrder(req, resp) {
        const ip = requestIP(req);
        const newOrder = req.body;
        const originOrder = _travelOrderService.emptyOrder();
        // flight seats validation
        _travelOrderService.orderFlightDataValidation(newOrder);
        _orderFlightSeatsValidation(ip, originOrder, newOrder);
        // flight seats update
        _orderFlightSeatsUpdate(ip, newOrder, false);
        // add order
        _travelOrderService.addOrder(ip, newOrder);
        // orders
        const orderDetails = _orderInCart(req);
        successResponse(resp, orderDetails);
    },
    updateOrder(req, resp) {
        const ip = requestIP(req);
        const updateOrder = req.body;
        const originOrder = _travelOrderService.order(ip, updateOrder.id);
        // flight seats validation
        _travelOrderService.orderFlightDataValidation(updateOrder);
        _orderFlightSeatsValidation(ip, originOrder, updateOrder);
        // flight seats update: remove origin seats / add new origin seats
        _orderFlightSeatsUpdate(ip, originOrder, true);
        _orderFlightSeatsUpdate(ip, updateOrder, false);
        // update order
        _travelOrderService.updateOrder(ip, updateOrder);
        // orders
        const orderDetails = _orderInCart(req);
        successResponse(resp, orderDetails);
    },
    cancelOrder(req, resp) {
        const ip = requestIP(req);
        const orderID = req.params['orderID'];
        const originOrder = _travelOrderService.order(ip, orderID);
        // remove origin seats
        _orderFlightSeatsUpdate(ip, originOrder, true);
        // remove order
        _travelOrderService.cancelOrder(ip, orderID);
        // orders
        const orderDetails = _orderInCart(req);
        successResponse(resp, orderDetails);
    },
    pay(req, resp) {
        const ip = requestIP(req);
        const orderIDList = req.body;
        const discount = _travelOrderService.getDiscount(ip);
        const usingDiscount = req.query['discount'] === 'true';
        const batchID = `TBC${new Date().getTime()}`;
        // sum of total amount
        const orderList = orderIDList
            .map((orderID) => _travelOrderService.order(ip, orderID));
        let totalAmount = orderList
            .map(({ productID, cardID, guestCount }) => [guestCount, _travelProductService.product(ip, productID || cardID)])
            .map(([guestCount, { price }]) => price * guestCount)
            .reduce((sum, current) => sum + current, 0);
        // using discount if necessary
        if (discount > 0 && usingDiscount) {
            totalAmount = totalAmount * discount;
            _travelOrderService.updateDiscount(ip, 'used', batchID);
        }
        // update order status
        orderList
            .forEach(({ id, productID, cardID }) => {
                const product =  _travelProductService.product(ip, productID || cardID);
                _travelOrderService.payed(ip, id, product.price, batchID);
            });
        successResponse(resp, batchID);
    },
    checkDiscount(req, resp) {
        const discount = _travelOrderService.getDiscount(requestIP(req));
        const result = discount === null;
        successResponse(resp, result);
    },
    getDiscount(req, resp) {
        const result = _travelOrderService.getDiscount(requestIP(req)) || 1;
        successResponse(resp, result);
    },
    addDiscount(req, resp) {
        _travelOrderService.updateDiscount(requestIP(req), 'add');
        successResponse(resp, {});
    },
};
