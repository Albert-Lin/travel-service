const { errorResponse, requestIP } = require('../utils/helpers');

function globalErrorHandler(error, req, resp, next) {
    const now = new Date();
    const log = {
        time: now.toLocaleString(),
        timestamp: now.getTime(),
        ip: req.ip,
        api: `${req.method} : ${req.path}`,
        data: {
            params: JSON.stringify(req.params),
            queryString: JSON.stringify(req.query),
            body: JSON.stringify(req.body),
        },
        error,
    };
    console.log('------------------');
    console.log(log);
    console.log('------------------');
    errorResponse(resp, error, now.getTime(), 500, 500);
}

module.exports = [
    globalErrorHandler,
];
