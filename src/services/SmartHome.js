const _rooms = {
    '127.0.0.1': _defaultRooms(),
};

const _devices = {
    '127.0.0.1': _defaultDevices(),
};

const _musicTimer = {
    '127.0.0.1': {},
};

const _timer = {
    minute: {},
    hour: {},
    hourPassed: {},
};

const _deviceSetting = {
    'router': {
        'iconMap': {
            'mobile': `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M12 18h.01M8 21h8a2 2 0 002-2V5a2 2 0 00-2-2H8a2 2 0 00-2 2v14a2 2 0 002 2z" />
                        </svg>`,
        },
        'data': [
            {
                'device': 'mobile',
                'name': 'Smartphone',
            },
        ],
    },
    'music': {
        'cover': './asset/image/device/device-01.avif',
        'name': 'Music',
        'composer': 'Unknown',
        'default': {
            'range': {
                'min': 0,
                'max': 180,
            },
            'value': 0,
            'volume': {
                'range': {
                    'min': 0,
                    'max': 100,
                },
                'value': 20,
            },
            'status': false,
            'timer': undefined,
            'type': undefined,
        },
    },
    'tv': {
        'default': {
            'volume': {
                'range': {
                    'min': 0,
                    'max': 100,
                },
                'value': 20,
            },
        },
    },
    'light': {
        'default': {
            'range': {
                'min': 0,
                'max': 100,
            },
            'value': 80,
        },
    },
    'lamp': {
        'default': {
            'range': {
                'min': 0,
                'max': 100,
            },
            'value': 80,
        },
    },
    'robot-vacuum': {
        'time': {
            'range': {
                'h': {
                    'min': 0,
                    'max': 12,
                },
                'm': {
                    'min': 0,
                    'max': 59,
                },
            },
            'current': 'everyday',
            'options': {
                'everyday': {
                    'h': '09',
                    'm': '30',
                    'format': 'am',
                },
                'mo': {
                    'h': '09',
                    'm': '30',
                    'format': 'am',
                },
                'tu': {
                    'h': '09',
                    'm': '30',
                    'format': 'am',
                },
                'we': {
                    'h': '09',
                    'm': '30',
                    'format': 'am',
                },
                'th': {
                    'h': '09',
                    'm': '30',
                    'format': 'am',
                },
                'fr': {
                    'h': '09',
                    'm': '30',
                    'format': 'am',
                },
                'sa': {
                    'h': '09',
                    'm': '30',
                    'format': 'am',
                },
                'su': {
                    'h': '09',
                    'm': '30',
                    'format': 'am',
                },
            },
        },
        'mode': 1,
        'map': [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
        ],
    },
    'washing-machine': {
        'time': {
            'range': {
                'h': {
                    'min': 0,
                    'max': 12,
                },
                'm': {
                    'min': 0,
                    'max': 59,
                },
            },
            'current': 'only',
            'options': {
                'only': {
                    'h': '09',
                    'm': '30',
                    'format': 'am',
                },
            },
        },
        'water': {
            'range': {
                'min': 1,
                'max': 14,
            },
            'value': 8,
        },
        'mode': 0,
    },
    'dehumidifier': {
        'water': {
            'range': {
                'min': 30,
                'max': 90,
            },
            'value': 50,
        },
        'mode': 5,
        'default': {
            'range': {
                'min': 0,
                'max': 24,
            },
            'value': 0,
        },
    },
    'ac': {
        'temp': {
            'range': {
                'min': 16,
                'max': 30,
            },
            'value': 26,
        },
        'mode': 0,
        'speed': 0,
        'default': {
            'range': {
                'min': 0,
                'max': 12,
            },
            'value': 0,
        },
    },
    'fan': {
        'speed': 0,
        'default': {
            'range': {
                'min': 0,
                'max': 12,
            },
            'value': 0,
        },
    },
    'camera': {
        'switch': [
            true,
            true,
            true,
            false,
        ],
        'radio': 0,
        'default': {
            'range': {
                'min': 0,
                'max': 2,
            },
            'value': 1,
        },
    },
    'fridge': {
        'storage': {
            'current': 5,
            'options': [ 4, 0, 0, 5, 5, -18 ],
            'items': [ 0, 0, 0, 0, 0, 0 ],
        },
        'temp': {
            'range': {
                'min': -24,
                'max': 8,
            },
        },
    },
};

const DEVICE_TYPES_OF_UNABLE_AUTO_TURN_OFF = [ 'fridge', 'camera' ];

function _id(storage, prefix) {
    const storageCount = Object.keys(storage).length;
    return `${prefix}${storageCount.toString().padStart(2, '0')}`;
}

function _userKey(ip, loginID) {
    return `${ip}-${loginID}`;
}

function _initUserData(ip) {
    const loginID = new Date().getTime();
    const userKey = _userKey(ip, loginID);
    _rooms[userKey] = _defaultRooms();
    _devices[userKey] = _defaultDevices();
    _musicTimer[userKey] = {};
    clearInterval(_timer.minute[userKey]);
    clearInterval(_timer.hour[userKey]);
    _timer.minute[userKey] = setInterval(() => _updateUsage(userKey), 1000 * 10);
    _timer.hour[userKey] = setInterval(() => {
        _updateDailyHistory(userKey);
        _timer.hourPassed[userKey]++;
    }, 1000 * 60 * 60 * 2);
    _timer.hourPassed[userKey] = 0;

    return loginID;
}

function _turnOnDevices(userKey, deviceIDList) {
    const userDevices = _devices[userKey];
    deviceIDList.forEach((id) => {
        userDevices[id].status = true;
    });
}

function _turnOffDevices(userKey, deviceIDList, exceptSpecialType = false) {
    const userDevices = _devices[userKey];
    deviceIDList
        .map((id) => userDevices[id])
        .filter(({ type }) => !(exceptSpecialType && DEVICE_TYPES_OF_UNABLE_AUTO_TURN_OFF.includes(type)))
        .forEach((device) => {
            device.status = false;
            device.using = 0;
        });
}

function _defaultRooms() {
    return {
        'r00': {
            id: 'r00',
            type: 'living-room',
            name: 'Living Room',
            overView: {
                water: 0,
            },
            devices: [
                'd00', 'd01', 'd02', 'd03', 'd04',
                'd05', 'd06', 'd07', 'd08', 'd09',
                'd10',
            ],
            dailyHistory: [
                [
                    0, 0, 0, 0, 0, 0,
                    0, 0, null, null, null, null,
                ],
                [
                    313, 276, 271, 501, 546, 472,
                    421, 372, null, null, null, null,
                ],
            ],
        },
        'r01': {
            id: 'r01',
            type: 'dining-room',
            name: 'Dining Room',
            overView: {
                water: 0,
            },
            devices: [
                'd11', 'd12', 'd13',
            ],
            dailyHistory: [
                [
                    0, 0, 0, 0, 0, 0,
                    0, 0, null, null, null, null,
                ],
                [
                    120, 113, 110, 135, 160, 130,
                    145, 127, null, null, null, null,
                ],
            ],
        },
        'r02': {
            id: 'r02',
            type: 'kitchen',
            name: 'Kitchen',
            overView: {
                water: 218,
            },
            devices: [
                'd14', 'd15', 'd16',
            ],
            dailyHistory: [
                [
                    0, 0, 0, 0, 1.05, 0.18,
                    0.31, 0.06, null, null, null, null,
                ],
                [
                    211, 185, 185, 260, 270, 230,
                    255, 204, null, null, null, null,
                ],
            ],
        },
        'r03': {
            id: 'r03',
            type: 'home-office',
            name: 'Home Office',
            overView: {
                water: 0,
            },
            devices: [
                'd17', 'd18', 'd19', 'd20', 'd21',
                'd22','d23',
            ],
            dailyHistory: [
                [
                    0, 0, 0, 0, 0, 0,
                    0, 0, null, null, null, null,
                ],
                [
                    335, 315, 225, 225, 365, 445,
                    415, 397, null, null, null, null,
                ],
            ],
        },
        'r04': {
            id: 'r04',
            type: 'balcony',
            name: 'Balcony',
            overView: {
                water: 102,
            },
            devices: [
                'd24', 'd25', 'd26',
            ],
            dailyHistory: [
                [
                    0, 0, 0, 0, 0.35, 0.4,
                    0, 0, null, null, null, null,
                ],
                [
                    171, 156, 136, 133, 133, 133,
                    133, 133, null, null, null, null,
                ],
            ],
        },
        'r05': {
            id: 'r05',
            type: 'bedroom',
            name: 'Bedroom - M',
            overView: {
                water: 0,
            },
            devices: [
                'd27', 'd28', 'd29', 'd30', 'd31',
                'd32', 'd33', 'd34', 'd35',
            ],
            dailyHistory: [
                [
                    0, 0, 0, 0, 0, 0,
                    0, 0, null, null, null, null,
                ],
                [
                    230, 205, 205, 210, 188, 195,
                    185, 158, null, null, null, null,
                ],
            ],
        },
        'r06': {
            id: 'r06',
            type: 'bathroom',
            name: 'Bathroom - M',
            overView: {
                water: 365,
            },
            devices: [
                'd36', 'd37', 'd38', 'd39',
            ],
            dailyHistory: [
                [
                    1.2, 0.02, 0, 0.19, 0.71, 0.43,
                    0.03, 0.1, null, null, null, null,
                ],
                [
                    172, 157, 152, 132, 162, 155,
                    133, 153, null, null, null, null,
                ],
            ],
        },
        'r07': {
            id: 'r07',
            type: 'bedroom',
            name: 'Bedroom - A',
            overView: {
                water: 0,
            },
            devices: [
                'd40', 'd41', 'd42', 'd43', 'd44',
                'd45',
            ],
            dailyHistory: [
                [
                    0, 0, 0, 0, 0, 0,
                    0, 0, null, null, null, null,
                ],
                [
                    290, 185, 170, 165, 180, 160,
                    160, 160, null, null, null, null,
                ],
            ],
        },
        'r08': {
            id: 'r08',
            type: 'bedroom',
            name: 'Bedroom - R',
            overView: {
                water: 0,
            },
            devices: [
                'd46', 'd47', 'd48', 'd49', 'd50',
            ],
            dailyHistory: [
                [
                    0, 0, 0, 0, 0, 0,
                    0, 0, null, null, null, null,
                ],
                [
                    172, 167, 172, 162, 182, 142,
                    142, 142, null, null, null, null,
                ],
            ],
        },
        'r09': {
            id: 'r09',
            type: 'bathroom',
            name: 'Bathroom',
            overView: {
                water: 471,
            },
            devices: [
                'd51', 'd52', 'd53', 'd54', 'd55',
            ],
            dailyHistory: [
                [
                    1.58, 0.95, 0.07, 0.13, 0.58, 0.04,
                    0, 0.11, null, null, null, null,
                ],
                [
                    209, 164, 149, 134, 241, 152.5,
                    129, 159.5, null, null, null, null,
                ],
            ],
        },
    };
}

function _defaultDevices() {
    return {
        'd00': {
            id: 'd00',
            type: 'tv',
            name: 'Tv',
            roomID: 'r00',
            totalUsing: 50,
            using: 25,
            status: true,
            setting: {
                default: {
                    volume: {
                        range: {
                            min: 0,
                            max: 100,
                        },
                        value: 20,
                    },
                },
            },
        },
        'd01': {
            id: 'd01',
            type: 'music',
            name: 'Music',
            roomID: 'r00',
            totalUsing: 76,
            using: 40,
            status: true,
            setting: {
                cover: './asset/image/device/device-02.avif',
                name: 'Time',
                composer: 'Hans Zimmer',
                default: {
                    range: {
                        min: 0,
                        max: 280,
                    },
                    value: 12,
                    volume: {
                        range: {
                            min: 0,
                            max: 100,
                        },
                        value: 30,
                    },
                    status: false,
                    timer: undefined,
                    type: 'repeat',
                },
            },
        },
        'd02': {
            id: 'd02',
            type: 'robot-vacuum',
            name: 'Robot Vacuum',
            roomID: 'r00',
            totalUsing: 154,
            using: 154,
            status: true,
            setting: {
                time: {
                    range: {
                        h: {
                            min: 0,
                            max: 12,
                        },
                        m: {
                            min: 0,
                            max: 59,
                        },
                    },
                    current: 'everyday',
                    options: {
                        everyday: { h: '09', m: '30', format: 'am' },
                        mo: { h: '09', m: '00', format: 'am' },
                        tu: { h: '09', m: '20', format: 'am' },
                        we: { h: '10', m: '00', format: 'am' },
                        th: { h: '08', m: '50', format: 'am' },
                        fr: { h: '09', m: '00', format: 'am' },
                        sa: { h: '02', m: '40', format: 'pm' },
                        su: { h: '04', m: '10', format: 'pm' },
                    },
                },
                mode: 5,
                map: [ 1, 2, 5, 6 ],
            },
        },
        'd03': {
            id: 'd03',
            type: 'dehumidifier',
            name: 'Dehumidifier',
            roomID: 'r00',
            totalUsing: 180,
            using: 134,
            status: true,
            setting: {
                water: {
                    range: {
                        min: 30,
                        max: 90,
                    },
                    value: 54,
                },
                mode: 0,
                default: {
                    range: {
                        min: 0,
                        max: 24,
                    },
                    value: 4,
                },
            },
        },
        'd04': {
            id: 'd04',
            type: 'router',
            name: 'Router',
            roomID: 'r00',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                iconMap: {
                    'tv': `<svg xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256">
                                <rect width="256" height="256" fill="none"></rect>
                                <rect x="32" y="48" width="192" height="144" rx="16" transform="translate(256 240) rotate(180)" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></rect>
                                <line x1="160" y1="224" x2="96" y2="224" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"></line>
                            </svg>`,
                    'laptop': `<svg xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256">
                                    <rect width="256" height="256" fill="none"></rect>
                                    <path d="M40,176V72A16,16,0,0,1,56,56H200a16,16,0,0,1,16,16V176" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path>
                                    <path d="M24,176H232a0,0,0,0,1,0,0v16a16,16,0,0,1-16,16H40a16,16,0,0,1-16-16V176A0,0,0,0,1,24,176Z" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path>
                                    <line x1="144" y1="88" x2="112" y2="88" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line>
                                </svg>`,
                    'mobile': `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M12 18h.01M8 21h8a2 2 0 002-2V5a2 2 0 00-2-2H8a2 2 0 00-2 2v14a2 2 0 002 2z" />
                                </svg>`,
                },
                data: [
                    {
                        device: 'tv',
                        name: 'Tv',
                    },
                    {
                        device: 'laptop',
                        name: 'MacBook',
                    },
                    {
                        device: 'mobile',
                        name: 'iPhone',
                    },
                ],
            },
        },
        'd05': {
            id: 'd05',
            type: 'light',
            name: 'Ceiling Light',
            roomID: 'r00',
            totalUsing: 280,
            using: 145,
            status: true,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 34,
                },
            },
        },
        'd06': {
            id: 'd06',
            type: 'light',
            name: 'Track Light',
            roomID: 'r00',
            totalUsing: 100,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 72,
                },
            },
        },
        'd07': {
            id: 'd07',
            type: 'lamp',
            name: 'Lamp',
            roomID: 'r00',
            totalUsing: 184,
            using: 184,
            status: true,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 40,
                },
            },
        },
        'd08': {
            id: 'd08',
            type: 'fan',
            name: 'Fan',
            roomID: 'r00',
            totalUsing: 240,
            using: 0,
            status: false,
            setting: {
                speed: 0,
                default: {
                    range: {
                        min: 0,
                        max: 12,
                    },
                    value: 0,
                },
            },
        },
        'd09': {
            id: 'd09',
            type: 'ac',
            name: 'Ac',
            roomID: 'r00',
            totalUsing: 228,
            using: 65,
            status: true,
            setting: {
                temp: {
                    range: {
                        min: 16,
                        max: 30,
                    },
                    value: 26,
                },
                mode: 0,
                speed: 0,
                default: {
                    range: {
                        min: 0,
                        max: 12,
                    },
                    value: 4,
                },
            },
        },
        'd10': {
            id: 'd10',
            type: 'camera',
            name: 'Camera',
            roomID: 'r00',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                switch: [ true, false, true, false ],
                radio: 0,
                default: {
                    range: {
                        min: 0,
                        max: 2,
                    },
                    value: 1,
                },
            },
        },

        'd11': {
            id: 'd11',
            type: 'light',
            name: 'Light',
            roomID: 'r01',
            totalUsing: 120,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 68,
                },
            },
        },
        'd12': {
            id: 'd12',
            type: 'fan',
            name: 'Fan',
            roomID: 'r01',
            totalUsing: 80,
            using: 0,
            status: false,
            setting: {
                speed: 0,
                default: {
                    range: {
                        min: 0,
                        max: 12,
                    },
                    value: 0,
                },
            },
        },
        'd13': {
            id: 'd13',
            type: 'camera',
            name: 'Camera',
            roomID: 'r01',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                switch: [ true, false, true, false ],
                radio: 0,
                default: {
                    range: {
                        min: 0,
                        max: 2,
                    },
                    value: 0,
                },
            },
        },

        'd14': {
            id: 'd14',
            type: 'light',
            name: 'Light',
            roomID: 'r02',
            totalUsing: 120,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 90,
                },
            },
        },
        'd15': {
            id: 'd15',
            type: 'fridge',
            name: 'Fridge',
            roomID: 'r02',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                storage: {
                    current: 5,
                    options: [ 4, 0, 1, 5, 6, -18 ],
                    items: [ 4, 6, 5, 4, 3, 8 ],
                },
                temp: {
                    range: {
                        min: -24,
                        max: 8,
                    },
                },
            },
        },
        'd16': {
            id: 'd16',
            type: 'camera',
            name: 'Camera',
            roomID: 'r02',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                switch: [ true, false, true, false ],
                radio: 0,
                default: {
                    range: {
                        min: 0,
                        max: 2,
                    },
                    value: 1,
                },
            },
        },

        'd17': {
            id: 'd17',
            type: 'music',
            name: 'Music',
            roomID: 'r03',
            totalUsing: 42,
            using: 0,
            status: false,
            setting: {
                cover: './asset/image/device/device-03.avif',
                name: 'Chevaliers de Sangreal',
                composer: 'Hans Zimmer',
                default: {
                    range: {
                        min: 0,
                        max: 248,
                    },
                    value: 110,
                    volume: {
                        range: {
                            min: 0,
                            max: 100,
                        },
                        value: 25,
                    },
                    status: false,
                    timer: undefined,
                    type: undefined,
                },
            },
        },
        'd18': {
            id: 'd18',
            type: 'router',
            name: 'Router',
            roomID: 'r03',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                iconMap: {
                    'desktop': `<svg xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256">
                                    <rect width="256" height="256" fill="none"></rect>
                                    <rect x="32" y="48" width="192" height="144" rx="16" transform="translate(256 240) rotate(180)" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></rect>
                                    <line x1="160" y1="224" x2="96" y2="224" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line>
                                    <line x1="32" y1="152" x2="224" y2="152" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line>
                                    <line x1="128" y1="192" x2="128" y2="224" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line>
                                </svg>`,
                    'laptop': `<svg xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256">
                                    <rect width="256" height="256" fill="none"></rect>
                                    <path d="M40,176V72A16,16,0,0,1,56,56H200a16,16,0,0,1,16,16V176" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path>
                                    <path d="M24,176H232a0,0,0,0,1,0,0v16a16,16,0,0,1-16,16H40a16,16,0,0,1-16-16V176A0,0,0,0,1,24,176Z" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path>
                                    <line x1="144" y1="88" x2="112" y2="88" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line>
                                </svg>`,
                    'tablet': `<svg xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256">
                                    <rect width="256" height="256" fill="none"></rect>
                                    <rect x="32" y="48" width="192" height="160" rx="16" transform="translate(256) rotate(90)" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></rect>
                                    <line x1="48" y1="64" x2="208" y2="64" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line>
                                    <line x1="48" y1="192" x2="208" y2="192" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line>
                                </svg>`,
                    'mobile': `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M12 18h.01M8 21h8a2 2 0 002-2V5a2 2 0 00-2-2H8a2 2 0 00-2 2v14a2 2 0 002 2z" />
                                </svg>`,
                },
                data: [
                    {
                        device: 'desktop',
                        name: 'iMac',
                    },
                    {
                        device: 'laptop',
                        name: 'MacBook',
                    },
                    {
                        device: 'laptop',
                        name: 'Laptop',
                    },
                    {
                        device: 'tablet',
                        name: 'iPad Pro',
                    },
                    {
                        device: 'mobile',
                        name: 'Business Phone',
                    },
                ],
            },
        },
        'd19': {
            id: 'd19',
            type: 'light',
            name: 'Ceiling Light',
            roomID: 'r03',
            totalUsing: 138,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 80,
                },
            },
        },
        'd20': {
            id: 'd20',
            type: 'light',
            name: 'Spot Light',
            roomID: 'r03',
            totalUsing: 22,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 75,
                },
            },
        },
        'd21': {
            id: 'd21',
            type: 'ac',
            name: 'Ac',
            roomID: 'r03',
            totalUsing: 232,
            using: 76,
            status: true,
            setting: {
                temp: {
                    range: {
                        min: 16,
                        max: 30,
                    },
                    value: 27,
                },
                mode: 0,
                speed: 0,
                default: {
                    range: {
                        min: 0,
                        max: 12,
                    },
                    value: 0,
                },
            },
        },
        'd22': {
            id: 'd22',
            type: 'camera',
            name: 'Camera - left',
            roomID: 'r03',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                switch: [ true, false, true, false ],
                radio: 0,
                default: {
                    range: {
                        min: 0,
                        max: 2,
                    },
                    value: 1,
                },
            },
        },
        'd23': {
            id: 'd23',
            type: 'camera',
            name: 'Camera - right',
            roomID: 'r03',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                switch: [ true, false, true, false ],
                radio: 0,
                default: {
                    range: {
                        min: 0,
                        max: 2,
                    },
                    value: 1,
                },
            },
        },

        'd24': {
            id: 'd24',
            type: 'light',
            name: 'Wall Sconce',
            roomID: 'r04',
            totalUsing: 160,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 64,
                },
            },
        },
        'd25': {
            id: 'd25',
            type: 'lamp',
            name: 'Floor Lamp',
            roomID: 'r04',
            totalUsing: 128,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 56,
                },
            },
        },
        'd26': {
            id: 'd26',
            type: 'camera',
            name: 'Camera',
            roomID: 'r04',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                switch: [ true, false, true, true ],
                radio: 1,
                default: {
                    range: {
                        min: 0,
                        max: 2,
                    },
                    value: 2,
                },
            },
        },

        'd27': {
            id: 'd27',
            type: 'tv',
            name: 'Tv',
            roomID: 'r05',
            totalUsing: 80,
            using: 0,
            status: false,
            setting: {
                default: {
                    volume: {
                        range: {
                            min: 0,
                            max: 100,
                        },
                        value: 16,
                    },
                },
            },
        },
        'd28': {
            id: 'd28',
            type: 'music',
            name: 'Music',
            roomID: 'r05',
            totalUsing: 48,
            using: 0,
            status: false,
            setting: {
                cover: './asset/image/device/device-04.avif',
                name: 'Relaxing Music',
                composer: 'Kevin Kern',
                default: {
                    range: {
                        min: 0,
                        max: 3482,
                    },
                    value: 124,
                    volume: {
                        range: {
                            min: 0,
                            max: 100,
                        },
                        value: 15,
                    },
                    status: false,
                    timer: undefined,
                    type: 'repeat',
                },
            },
        },
        'd29': {
            id: 'd29',
            type: 'robot-vacuum',
            name: 'Robot Vacuum',
            roomID: 'r05',
            totalUsing: 96,
            using: 90,
            status: true,
            setting: {
                time: {
                    range: {
                        h: {
                            min: 0,
                            max: 12,
                        },
                        m: {
                            min: 0,
                            max: 59,
                        },
                    },
                    current: 'everyday',
                    options: {
                        everyday: { h: '09', m: '30', format: 'am' },
                        mo: { h: '09', m: '00', format: 'am' },
                        tu: { h: '09', m: '20', format: 'am' },
                        we: { h: '10', m: '00', format: 'am' },
                        th: { h: '08', m: '50', format: 'am' },
                        fr: { h: '09', m: '00', format: 'am' },
                        sa: { h: '02', m: '40', format: 'pm' },
                        su: { h: '04', m: '10', format: 'pm' },
                    },
                },
                mode: 0,
                map: [ 0, 3, 4 ],
            },
        },
        'd30': {
            id: 'd30',
            type: 'light',
            name: 'Wall Sconce - L',
            roomID: 'r05',
            totalUsing: 64,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 64,
                },
            },
        },
        'd31': {
            id: 'd31',
            type: 'light',
            name: 'Wall Sconce - R',
            roomID: 'r05',
            totalUsing: 80,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 64,
                },
            },
        },
        'd32': {
            id: 'd32',
            type: 'light',
            name: 'Ceiling Light',
            roomID: 'r05',
            totalUsing: 144,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 72,
                },
            },
        },
        'd33': {
            id: 'd33',
            type: 'fan',
            name: 'Fan',
            roomID: 'r05',
            totalUsing: 100,
            using: 0,
            status: false,
            setting: {
                speed: 0,
                default: {
                    range: {
                        min: 0,
                        max: 12,
                    },
                    value: 4,
                },
            },
        },
        'd34': {
            id: 'd34',
            type: 'ac',
            name: 'Ac',
            roomID: 'r05',
            totalUsing: 124,
            using: 0,
            status: false,
            setting: {
                temp: {
                    range: {
                        min: 16,
                        max: 30,
                    },
                    value: 26,
                },
                mode: 0,
                speed: 0,
                default: {
                    range: {
                        min: 0,
                        max: 12,
                    },
                    value: 6,
                },
            },
        },
        'd35': {
            id: 'd35',
            type: 'camera',
            name: 'Camera',
            roomID: 'r05',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                switch: [ true, false, true, false ],
                radio: 0,
                default: {
                    range: {
                        min: 0,
                        max: 2,
                    },
                    value: 2,
                },
            },
        },

        'd36': {
            id: 'd36',
            type: 'dehumidifier',
            name: 'Dehumidifier',
            roomID: 'r06',
            totalUsing: 152,
            using: 32,
            status: true,
            setting: {
                water: {
                    range: {
                        min: 30,
                        max: 90,
                    },
                    value: 56,
                },
                mode: 1,
                default: {
                    range: {
                        min: 0,
                        max: 24,
                    },
                    value: 2,
                },
            },
        },
        'd37': {
            id: 'd37',
            type: 'light',
            name: 'Washbasin',
            roomID: 'r06',
            totalUsing: 128,
            using: 128,
            status: true,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 60,
                },
            },
        },
        'd38': {
            id: 'd38',
            type: 'light',
            name: 'Bathtub',
            roomID: 'r06',
            totalUsing: 96,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 68,
                },
            },
        },
        'd39': {
            id: 'd39',
            type: 'camera',
            name: 'Camera',
            roomID: 'r06',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                switch: [ true, false, true, false ],
                radio: 0,
                default: {
                    range: {
                        min: 0,
                        max: 2,
                    },
                    value: 1,
                },
            },
        },

        'd40': {
            id: 'd40',
            type: 'music',
            name: 'Music',
            roomID: 'r07',
            totalUsing: 80,
            using: 0,
            status: false,
            setting: {
                cover: './asset/image/device/device-05.avif',
                name: 'Cello Suite No. 1',
                composer: 'Bach',
                default: {
                    range: {
                        min: 0,
                        max: 212,
                    },
                    value: 86,
                    volume: {
                        range: {
                            min: 0,
                            max: 100,
                        },
                        value: 28,
                    },
                    status: false,
                    timer: undefined,
                    type: undefined,
                },
            },
        },
        'd41': {
            id: 'd41',
            type: 'light',
            name: 'Light',
            roomID: 'r07',
            totalUsing: 140,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 64,
                },
            },
        },
        'd42': {
            id: 'd42',
            type: 'lamp',
            name: 'Lamp',
            roomID: 'r07',
            totalUsing: 136,
            using: 136,
            status: true,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 52,
                },
            },
        },
        'd43': {
            id: 'd43',
            type: 'fan',
            name: 'Fan',
            roomID: 'r07',
            totalUsing: 120,
            using: 0,
            status: false,
            setting: {
                speed: 0,
                default: {
                    range: {
                        min: 0,
                        max: 12,
                    },
                    value: 0,
                },
            },
        },
        'd44': {
            id: 'd44',
            type: 'ac',
            name: 'Ac',
            roomID: 'r07',
            totalUsing: 154,
            using: 0,
            status: false,
            setting: {
                temp: {
                    range: {
                        min: 16,
                        max: 30,
                    },
                    value: 24,
                },
                mode: 0,
                speed: 1,
                default: {
                    range: {
                        min: 0,
                        max: 12,
                    },
                    value: 0,
                },
            },
        },
        'd45': {
            id: 'd45',
            type: 'camera',
            name: 'Camera',
            roomID: 'r07',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                switch: [ true, false, true, false ],
                radio: 0,
                default: {
                    range: {
                        min: 0,
                        max: 2,
                    },
                    value: 2,
                },
            },
        },

        'd46': {
            id: 'd46',
            type: 'music',
            name: 'Music',
            roomID: 'r08',
            totalUsing: 80,
            using: 0,
            status: false,
            setting: {
                cover: './asset/image/device/device-06.avif',
                name: 'Dynamite',
                composer: 'BTS',
                default: {
                    range: {
                        min: 0,
                        max: 223,
                    },
                    value: 24,
                    volume: {
                        range: {
                            min: 0,
                            max: 100,
                        },
                        value: 20,
                    },
                    status: false,
                    timer: undefined,
                    type: undefined,
                },
            },
        },
        'd47': {
            id: 'd47',
            type: 'lamp',
            name: 'Pendant Lamp',
            roomID: 'r08',
            totalUsing: 104,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 60,
                },
            },
        },
        'd48': {
            id: 'd48',
            type: 'fan',
            name: 'Fan',
            roomID: 'r08',
            totalUsing: 126,
            using: 0,
            status: false,
            setting: {
                speed: 0,
                default: {
                    range: {
                        min: 0,
                        max: 12,
                    },
                    value: 0,
                },
            },
        },
        'd49': {
            id: 'd49',
            type: 'ac',
            name: 'Ac',
            roomID: 'r08',
            totalUsing: 131,
            using: 0,
            status: false,
            setting: {
                temp: {
                    range: {
                        min: 16,
                        max: 30,
                    },
                    value: 27,
                },
                mode: 0,
                speed: 0,
                default: {
                    range: {
                        min: 0,
                        max: 12,
                    },
                    value: 4,
                },
            },
        },
        'd50': {
            id: 'd50',
            type: 'camera',
            name: 'Camera',
            roomID: 'r08',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                switch: [ true, false, true, false ],
                radio: 0,
                default: {
                    range: {
                        min: 0,
                        max: 2,
                    },
                    value: 1,
                },
            },
        },

        'd51': {
            id: 'd51',
            type: 'washing-machine',
            name: 'Washing Machine',
            roomID: 'r09',
            totalUsing: 180,
            using: 20,
            status: true,
            setting: {
                time: {
                    range: {
                        h: {
                            min: 0,
                            max: 12,
                        },
                        m: {
                            min: 0,
                            max: 59,
                        },
                    },
                    current: 'only',
                    options: { only: { h: '2', m: '30', format: 'pm' } },
                },
                water: {
                    range: {
                        min: 1,
                        max: 14,
                    },
                    value: 8,
                },
                mode: 0,
            },
        },
        'd52': {
            id: 'd52',
            type: 'dehumidifier',
            name: 'Dehumidifier',
            roomID: 'r09',
            totalUsing: 182,
            using: 81,
            status: true,
            setting: {
                water: {
                    range: {
                        min: 30,
                        max: 90,
                    },
                    value: 52,
                },
                mode: 5,
                default: {
                    range: {
                        min: 0,
                        max: 24,
                    },
                    value: 2,
                },
            },
        },
        'd53': {
            id: 'd53',
            type: 'light',
            name: 'Washbasin',
            roomID: 'r09',
            totalUsing: 120,
            using: 107,
            status: true,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 62,
                },
            },
        },
        'd54': {
            id: 'd54',
            type: 'light',
            name: 'Shower',
            roomID: 'r09',
            totalUsing: 16,
            using: 0,
            status: false,
            setting: {
                default: {
                    range: {
                        min: 0,
                        max: 100,
                    },
                    value: 74,
                },
            },
        },
        'd55': {
            id: 'd55',
            type: 'camera',
            name: 'Camera',
            roomID: 'r09',
            totalUsing: 840,
            using: 840,
            status: true,
            setting: {
                switch: [ true, false, true, false ],
                radio: 0,
                default: {
                    range: {
                        min: 0,
                        max: 2,
                    },
                    value: 1,
                },
            },
        },
    };
}

function _stopMusic(setting, userKey, deviceID, type) {
    clearInterval(_musicTimer[userKey][deviceID]);
    setting.default.timer = undefined;
    setting.default.status = false;
    if (type === 'stop') {
        setting.default.value = 0;
    }
}

function _musicPlayer(setting, userKey, deviceID) {
    setting.default.value++;
    const isFinished = setting.default.value > setting.default.range.max;
    if (isFinished) {
        setting.default.value = 0;
    }

    const toRepeat = setting.default.type === 'repeat';
    if (isFinished && !toRepeat) {
        _stopMusic(setting, userKey, deviceID);
    }
}

function _playMusic(setting, userKey, deviceID) {
    _musicTimer[userKey][deviceID] = setInterval(() => _musicPlayer(setting, userKey, deviceID), 1000 * 1);
    setting.default.status = true;
}

function _updateUsage(userKey) {
    Object
        .values(_devices[userKey])
        .filter(({ status, isDelete }) => status && !isDelete)
        .forEach((device) => {
            device.using += (10 / 60);
            device.totalUsing += (10 / 60);
        });
}

function _singleRoomPowerTotalUsage(userKey, roomID) {
    let sum = 0;
    const names = [];
    const totalUsingList = Object
        .values(_devices[userKey])
        .filter((device) => device.roomID === roomID)
        .map(({ name, totalUsing, isDelete }) => {
            sum += totalUsing;
            return { name, totalUsing, isDelete };
        })
        .filter(({ isDelete }) => !isDelete)
        .map(({ name, totalUsing }) => {
            names.push(name);
            return totalUsing;
        });
    return { names, totalUsingList, sum };
}

function _updateDailyHistory(userKey) {
    let usingServiceOver12Hours = false;
    Object
        .values(_rooms[userKey])
        .forEach(({ id, dailyHistory: [ dailyWaterHistory, dailyPowerHistory ] }) => {
            const { sum } = _singleRoomPowerTotalUsage(userKey, id);
            const index = dailyPowerHistory.indexOf(null);
            usingServiceOver12Hours = index === -1;
            if (!usingServiceOver12Hours) {
                const sumOfBeforeLastHour = dailyPowerHistory.slice(0, index).reduce((s, c) => s + c, 0);
                dailyPowerHistory[index] = sum - sumOfBeforeLastHour;
                dailyWaterHistory[index] = 0;
            }
        });

    if (usingServiceOver12Hours) {
        _initUserData(userKey);
    }
}

module.exports = {
    // init:
    initUserData: _initUserData,
    userKey: _userKey,
    terminate(userKey) {
        delete _rooms[userKey];
        delete _devices[userKey];
        delete _musicTimer[userKey];
        clearInterval(_timer.minute[userKey]);
        clearInterval(_timer.hour[userKey]);
        delete _timer.minute[userKey];
        delete _timer.hour[userKey];
        delete _timer.hourPassed[userKey];
    },
    // room:
    room(userKey, id) {
        return _rooms[userKey][id];
    },
    addRoom(userKey, setting) {
        const userRooms = _rooms[userKey];
        const id = _id(userRooms, 'r');
        const passedHourCount = 8 + _timer.hourPassed[userKey];
        const unReachHourCount = 12 - passedHourCount;
        const history = [
            ...(new Array(passedHourCount).fill(0)),
            ...(new Array(unReachHourCount).fill(null)),
        ];
        userRooms[id] = {
            ...setting,
            id,
            dailyHistory: [
                [ ...history ],
                [ ...history ],
            ]
        };
        return userRooms[id];
    },
    removeRoom(userKey, roomID) {
        const userRooms = _rooms[userKey];
        const userDevices = _devices[userKey];
        userRooms[roomID]
            .devices
            .forEach((deviceID) => delete userDevices[deviceID]);
        delete userRooms[roomID];
    },
    rooms(userKey, idList) {
        const userRooms = _rooms[userKey];
        const getAll = !idList || idList.length === 0;
        return getAll
            ? Object.values(userRooms)
            : idList.map((id) => userRooms[id]);
    },
    // device:
    device(userKey, id) {
        const userDevices = _devices[userKey];
        return userDevices[id];
    },
    addDevice(userKey, deviceData) {
        const userRooms = _rooms[userKey];
        const userDevices = _devices[userKey];
        const id = _id(userDevices, 'd');
        const type = deviceData.type;
        userDevices[id] = {
            ...deviceData,
            id,
            totalUsing: 0,
            using: 0,
            status: false,
            setting: _deviceSetting[type],
        };
        userRooms[deviceData.roomID].devices.push(id);
        return userDevices[id];
    },
    updateDeviceSetting(userKey, deviceData) {
        const userDevices = _devices[userKey];
        const userDevice = userDevices[deviceData.id];
        userDevice.setting = deviceData.setting;
        return userDevice;
    },
    removeDevice(userKey, deviceID) {
        const userRooms = _rooms[userKey];
        const userDevices = _devices[userKey];
        _turnOffDevices(userKey, [deviceID]);
        const device = userDevices[deviceID];
        const room = userRooms[device.roomID];
        const deviceIndex = room.devices.indexOf(deviceID);
        room.devices.splice(deviceIndex, 1);
        userDevices[deviceID].isDelete = true;
        const isMusic = device.type === 'music';
        if (isMusic) {
            clearInterval(_musicTimer[userKey][deviceID] || -1);
            delete _musicTimer[userKey][deviceID]
        }
    },
    devices(userKey, roomID) {
        const userRooms = _rooms[userKey];
        const userDevices = _devices[userKey];
        const deviceIDList = userRooms[roomID]
            ? userRooms[roomID].devices || []
            : [];
        return deviceIDList
            .map((id) => userDevices[id])
            .filter(({ isDelete }) => !isDelete);
    },
    activeDevices(userKey, devices) {
        const userDevices = _devices[userKey];
        return devices.filter((device) => {
            const isID = typeof device === 'string';
            device = isID ? userDevices[device] : device;
            return device.status;
        });
    },
    turnOnDevices: _turnOnDevices,
    turnOffDevices: _turnOffDevices,
    playMusic(userKey, deviceID) {
        const device = _devices[userKey][deviceID];
        _playMusic(device.setting, userKey, deviceID);
    },
    stopMusic(userKey, deviceID, type) {
        const device = _devices[userKey][deviceID];
        _stopMusic(device.setting, userKey, deviceID, type);
    },
    updateMusic(userKey, deviceID, setting) {
        const { keyPath, value } = setting;
        const device = _devices[userKey][deviceID];
        let object = device.setting;
        const keys = keyPath.split('.');
        keys.forEach((key, index) => {
            const atLast = index === keys.length - 1;
            if (atLast) {
                object[key] = value;
            } else {
                object = object[key];
            }
        });
    },
    // others
    singleRoomPowerTotalUsage: _singleRoomPowerTotalUsage,
    DEVICE_TYPES_OF_UNABLE_AUTO_TURN_OFF,
};
