const _orders = {
    // '127.0.0.1': {
    //     'o00': {
    //         id: 'o00',
    //         productID: 'c00',
    //         flightID: 'f00',
    //         guestCount: 0,
    //         departure: {
    //             timestamp: 0,
    //             seats: [],
    //         },
    //         arrival: {
    //             timestamp: 0,
    //             seats: [],
    //         },
    //         payed: false,
    //     }
    // }
};

const _discounts = {
    // '127.0.0.1': null, // null, 1, 0.95
};

const _orderBatches = {
    // '127.0.0.1': {
    //     'timestamp': {
    //         orders: [
    //             { id: '', price: 0 }
    //         ],
    //         useDiscount: false,
    //     }
    // }
};

function _id(storage, prefix) {
    const storageCount = Object.keys(storage).length;
    const lastKey = Object.keys(storage)[ storageCount - 1 ] || '00';
    return `${prefix}${( Number(lastKey.replace(prefix, '')) + 1 ).toString().padStart(2, '0')}`;
}

function _defaultOrders() {
    const today = (new Date()).getTime();
    const tomorrow = today + 86400000 * 1;
    const date30daysAfter = today + 86400000 * 30;

    return {
        'o00': {
            id: 'o00',
            productID: 'c15',
            flightID: 't15',
            guestCount: 4,
            departure: {
                timestamp: tomorrow,
                seats: [ '1A', '1C', '1D', '3D' ],
            },
            arrival: {
                timestamp: tomorrow + 86400000 * 3,
                seats: [ '3A', '2C', '2D', '3D' ],
            },
            payed: false,
        },
        'o01': {
            id: 'o01',
            productID: 'c05',
            flightID: 't05',
            guestCount: 1,
            departure: {
                timestamp: date30daysAfter,
                seats: [ '2C' ],
            },
            arrival: {
                timestamp: date30daysAfter + 86400000 * 30,
                seats: [ '5D' ],
            },
            payed: false,
        }
    };
}

function _userOrders(ip) {
    if (!_orders[ip]) {
        _orders[ip] = _defaultOrders();
    }
    return _orders[ip];
}

function _userOrderBatch(ip, batchID, withVerify) {
    if (!_orderBatches[ip]) {
        _orderBatches[ip] = {};
    }

    if (!_orderBatches[ip][batchID]) {
        if (withVerify) {
            throw new Error(`Cannot Found Payment Record of ${batchID}`);
        }

        _orderBatches[ip][batchID] = {
            orders: [],
            useDiscount: false,
        };
    }

    return _orderBatches[ip][batchID];
}

function _updateProductID(origin, meta) {
    if (!meta.cardID && !meta.productID) {
        throw Error(`Order cannot without cardID`)
    }
    origin.productID = meta.cardID || meta.productID;
}

function _updateTicketID(origin, meta) {
    if (!meta.ticketID && !meta.flightID) {
        throw Error(`Order cannot without ticketID`)
    }
    origin.flightID = meta.ticketID || meta.flightID;
}


function _orderFlightDataValidation(meta) {
    if (!meta.departure || !meta.departure.timestamp || !meta.departure.seats) {
        throw Error('Order cannot without departure data');
    }
    if (!meta.arrival || !meta.arrival.seats) {
        throw Error('Order cannot without arrival data');
    }
    if (meta.guestCount !== meta.departure.seats.length || meta.guestCount !== meta.arrival.seats.length) {
        throw Error('Guest count order should equals to seats count');
    }
}

function _updateGuestCount(origin, meta) {
    if (!meta.guestCount) {
        throw Error('Order cannot without guest count');
    }
    _orderFlightDataValidation(meta);

    origin.guestCount = meta.guestCount;
    origin.departure = meta.departure;
    origin.arrival = meta.arrival;
}

module.exports = {
    init(ip) {
        delete _discounts[ip];
        delete _orders[ip];
        delete _orderBatches[ip];
    },
    orders(ip) {
        return Object.values(_userOrders(ip));
    },
    ordersOf(ip, payed) {
        return Object
            .values(_userOrders(ip))
            .filter((order) => order.payed === payed);
    },
    batchOrders(ip, batchID) {
        return _userOrderBatch(ip, batchID, true);
    },
    order(ip, id) {
        return _userOrders(ip)[id];
    },
    orderFlightDataValidation: _orderFlightDataValidation,
    addOrder(ip, meta) {
        const orders = _userOrders(ip);
        const id = _id(orders, 'o');
        const newOrder = { id, payed: false };
        _updateProductID(newOrder, meta);
        _updateTicketID(newOrder, meta);
        _updateGuestCount(newOrder, meta);
        orders[id] = newOrder;
    },
    updateOrder(ip, meta) {
        const order = _userOrders(ip)[meta.id];
        if (order.payed) {
            throw Error(`Cannot update payed order (${order.id})`);
        }
        _updateGuestCount(order, meta);
    },
    cancelOrder(ip, id) {
        const order = _userOrders(ip)[id];
        if (order.payed) {
            throw Error(`Cannot cancel payed order (${order.id})`);
        }
        delete _userOrders(ip)[id];
    },
    payed(ip, id, price, batchID) {
        const order = _userOrders(ip)[id];
        if (!order.payed) {
            const orderBatch = _userOrderBatch(ip, batchID);
            order.payed = true;
            orderBatch.orders.push({
                id: order.id,
                price: price * order.guestCount,
            });
        }
    },
    getDiscount(ip) {
        return _discounts[ip] || null
    },
    updateDiscount(ip, action, batchID) {
        const isExist = _discounts.hasOwnProperty(ip);
        if (action === 'add' && !isExist) {
            _discounts[ip] = 0.95;
        }
        if (action === 'used') {
            const orderBatch = _userOrderBatch(ip, batchID);
            _discounts[ip] = 1;
            orderBatch.useDiscount = true;
        }
    },
    emptyOrder() {
        return {
            id: null,
            productID: null,
            flightID: null,
            guestCount: 0,
            departure: {
                timestamp: 0,
                seats: [],
            },
            arrival: {
                timestamp: 0,
                seats: [],
            },
            payed: false,
        };
    }
};
