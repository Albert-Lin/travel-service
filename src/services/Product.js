const { copy } = require('../utils/helpers');

const _PRODUCTS = {
    'c00': {
        id: 'c00',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/iceland-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/iceland-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/iceland-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/iceland-4.avif',
                style: '',
            },
        ],
        country: `Iceland`,
        location: `Thórsmörk, Jökulsárlón, Grindavíkurbær, Fjaðrárgljúfur`,
        describe: `Iceland is a Nordic island country in the North Atlantic Ocean and the most sparsely populated country in Europe. The interior consists of a plateau characterised by sand and lava fields, mountains, and glaciers, and many glacial rivers flow to the sea through the lowlands. Many species of fish live in the ocean waters surrounding Iceland, and the fishing industry is a major part of Iceland's economy.`,
        rate: '4.9',
        price: 18800,
        isFavorite: false,
        reviews: {
            transportation: '4.9',
            hotel: '5.0',
            food: '4.8',
            service: '4.9',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1739460.2951123363!2d-21.487267545487757!3d64.80143351407416!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48d22b52a3eb6043%3A0x6f8a0434e5c1459a!2sIceland!5e0!3m2!1sen!2stw!4v1653875159712!5m2!1sen!2stw`,
        tag: [ 'nature', '冰島', '自然', ],
    },
    'c01': {
        id: 'c01',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/norway-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/norway-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/norway-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/norway-4.avif',
                style: '',
            },
        ],
        country: `Norway`,
        location: `Bryggen, Tromsø, Lofoten Islands`,
        describe: `Norway has an extensive coastline, the maritime influence dominates Norway's climate, with mild lowland temperatures on the sea coasts; the interior, while colder, is also a lot milder than areas elsewhere in the world on such northerly latitudes. Norway having extensive reserves of petroleum, natural gas, minerals, lumber, seafood, and fresh water.`,
        rate: '4.7',
        price: 16500,
        isFavorite: false,
        reviews: {
            transportation: '4.6',
            hotel: '4.7',
            food: '5.0',
            service: '4.5',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7087980.546526296!2d8.778785214461081!3d64.29612659703614!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x461268458f4de5bf%3A0xa1b03b9db864d02b!2sNorway!5e0!3m2!1sen!2stw!4v1653833108351!5m2!1sen!2stw`,
        tag: [ 'nature', 'culture', 'building', 'food', '挪威', '自然', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c02': {
        id: 'c02',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/switzerland-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/switzerland-2.avif',
                style: 'filter: brightness(120%);',
            },
            {
                src: './asset/image/card/europe/switzerland-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/switzerland-4.avif',
                style: '',
            },
        ],
        country: `Switzerland`,
        location: `Mürren, Alps, Mümliswil-Ramiswil`,
        describe: `Switzerland is a landlocked country at the confluence of Western, Central and Southern Europe, and has four national languages. Its cities such as Zürich, Geneva and Basel rank among the highest in the world in terms of quality of life, these three cities are home of international organisations such as the WTO, the WHO.`,
        rate: '5.0',
        price: 19800,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '5.0',
            food: '5.0',
            service: '5.0',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1398134.5100366022!2d7.103215952781818!3d46.80959537615865!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478c64ef6f596d61%3A0x5c56b5110fcb7b15!2sSwitzerland!5e0!3m2!1sen!2stw!4v1653831678678!5m2!1sen!2stw`,
        tag: [ 'nature', 'food', '瑞士', '自然', '食物', '美食', ],
    },
    'c03': {
        id: 'c03',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/austria-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/austria-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/austria-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/austria-4.avif',
                style: '',
            },
        ],
        country: `Austria`,
        location: `Vienna, Hallstatt, Sankt Gilgen, Leutasch`,
        describe: `Austria is a country in the southern part of Central Europe, one of the countries with the highest standard of living. Austria's past as a European power and its cultural environment generated a broad contribution to various forms of art, most notably among them music. Austria was the birthplace of many famous composers, and made Vienna the European capital of classical music.`,
        rate: '4.8',
        price: 17800,
        isFavorite: false,
        reviews: {
            transportation: '4.8',
            hotel: '4.7',
            food: '5.0',
            service: '4.7',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1375399.3931691782!2d13.076213706097414!3d47.678070284317045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x476d079b259d2a7f%3A0x1012d47bdde4c1af!2sAustria!5e0!3m2!1sen!2stw!4v1653833257306!5m2!1sen!2stw`,
        tag: [ 'nature', 'culture', 'building', '奧地利', '自然', '文化', '人文', '建築', ],
    },
    'c04': {
        id: 'c04',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/germany-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/germany-2.avif',
                style: 'filter: brightness(120%) contrast(80%);',
            },
            {
                src: './asset/image/card/europe/germany-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/germany-4.avif',
                style: 'filter: brightness(120%);',
            },
        ],
        country: `Germany`,
        location: `Berlin, Rothenburg ob der Tauber, Neuschwanstein`,
        describe: `Germany is a country in Central Europe. It has the largest economy in Europe, the world's fourth-largest economy by nominal GDP. Germany has been called 'The land of poets and thinkers', because of the major role its scientists, writers and philosophers have played in the development of Western thought. It has the third-greatest number of UNESCO World Heritage Sites.`,
        rate: '4.7',
        price: 18800,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.5',
            food: '4.7',
            service: '4.6',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5131527.7280097045!2d5.969026796597606!3d51.0968581868216!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479a721ec2b1be6b%3A0x75e85d6b8e91e55b!2sGermany!5e0!3m2!1sen!2stw!4v1653833454952!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', 'food', '德國', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c05': {
        id: 'c05',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/belgium-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/belgium-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/belgium-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/belgium-4.avif',
                style: '',
            },
        ],
        country: `Belgium`,
        location: `Brussels, Bruges`,
        describe: `Belgium is a country in Northwestern Europe. It is a developed country, with an advanced high-income economy. It has very high standards of living, quality of life, healthcare, education, its linguistic diversity are reflected in its complex system of governance, made up of six different governments. Belgium is famous for beer, chocolate, waffles and French fries.`,
        rate: '4.7',
        price: 17400,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.5',
            food: '4.7',
            service: '4.6',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1299443.2092460003!2d3.345156995861258!3d50.49742270783183!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c17d64edf39797%3A0x47ebf2b439e60ff2!2sBelgium!5e0!3m2!1sen!2stw!4v1653875778113!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', 'food', '比利時', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c06': {
        id: 'c06',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/france-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/france-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/france-3.avif',
                style: 'filter: brightness(120%);',
            },
            {
                src: './asset/image/card/europe/france-4.avif',
                style: '',
            },
        ],
        country: `France`,
        location: `Paris`,
        describe: `France has been a center of Western cultural development for centuries. It receives the highest number of tourists per year, largely thanks to the numerous cultural establishments and historical buildings implanted all over the territory. It hosts the fifth-largest number of UNESCO World Heritage Sites. France is a developed country with the world's seventh-largest economy by nominal GDP.`,
        rate: '4.8',
        price: 17200,
        isFavorite: false,
        reviews: {
            transportation: '4.7',
            hotel: '4.8',
            food: '4.7',
            service: '5.0',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5661876.551996695!2d-2.435119738363391!3d46.13903298857387!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd54a02933785731%3A0x6bfd3f96c747d9f7!2sFrance!5e0!3m2!1sen!2stw!4v1653833559154!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', 'food', '法國', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c07': {
        id: 'c07',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/spain-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/spain-2.avif',
                style: 'object-position: bottom; background-position: bottom;',
            },
            {
                src: './asset/image/card/europe/spain-3.avif',
                style: 'object-position: bottom; background-position: bottom;',
            },
            {
                src: './asset/image/card/europe/spain-4.avif',
                style: '',
            },
        ],
        country: `Spain`,
        location: `Barcelona, Calpe`,
        describe: `Spain is a country in southwestern Europe and is the largest country in Southern Europe. Spanish art, music, literature and cuisine have been influential worldwide, particularly in Western Europe and the Americas, and making Spanish the world's second-most spoken native language. It has the world's fourth-largest number of World Heritage Sites and is the world's second-most visited country.`,
        rate: '4.9',
        price: 17500,
        isFavorite: false,
        reviews: {
            transportation: '4.9',
            hotel: '5.0',
            food: '4.9',
            service: '4.8',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6247515.049248458!2d-8.201854992922032!3d40.130154352119675!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xc42e3783261bc8b%3A0xa6ec2c940768a3ec!2sSpain!5e0!3m2!1sen!2stw!4v1653833753742!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', 'food', '西班牙', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c08': {
        id: 'c08',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/italy-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/italy-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/italy-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/italy-4.avif',
                style: '',
            },
        ],
        country: `Italy`,
        location: `Pisa, Rome, Manarola`,
        describe: `Italy location in Southern Europe and the Mediterranean, is considered one of the birthplaces of western civilization and a cultural superpower. The country has long been a global center of art, music, literature, philosophy, science and technology, and fashion. It has the world's largest number of World Heritage Sites, and is the fifth-most visited country.`,
        rate: '4.8',
        price: 16800,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.8',
            food: '4.7',
            service: '4.7',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6017708.707940513!2d8.39944510545708!3d42.56942574410885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12d4fe82448dd203%3A0xe22cf55c24635e6f!2sItaly!5e0!3m2!1sen!2stw!4v1653833841170!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', 'food', '義大利', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c09': {
        id: 'c09',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/vatican-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/vatican-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/vatican-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/vatican-4.avif',
                style: '',
            },
        ],
        country: `Vatican`,
        location: `Vatican`,
        describe: `Vatican is an independent city-state surrounded by Rome, and is the smallest state in the world. It is an ecclesiastical ruled by the pope who is the bishop of Rome and head of the Catholic Church. Within Vatican are religious and cultural sites such as St. Peter's Basilica, the Sistine Chapel, and the Vatican Museums. They feature some of the world's most famous paintings and sculptures.`,
        rate: '4.6',
        price: 15000,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.2',
            food: '4.6',
            service: '4.6',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5938.988151003651!2d12.448906915152815!3d41.903736415649675!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1325890a57d42d3d%3A0x94f9ab23a7eb0!2s00120%20Vatican%20City!5e0!3m2!1sen!2stw!4v1653833939728!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', '梵蒂岡', '文化', '人文', '建築', ],
    },
    'c10': {
        id: 'c10',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/unitedKingdom-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/unitedKingdom-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/unitedKingdom-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/unitedKingdom-4.avif',
                style: '',
            },
        ],
        country: `United Kingdom`,
        location: `London`,
        describe: `United Kingdom is surrounded by the Atlantic Ocean. It is a constitutional monarchy. The monarch, Queen Elizabeth II, has reigned since 1952. United Kingdom became the world's first industrialised country. Today United Kingdom remains one of the world's great powers, with considerable economic, cultural, military, scientific, technological and political influence internationally.`,
        rate: '4.4',
        price: 17000,
        isFavorite: false,
        reviews: {
            transportation: '3.8',
            hotel: '4.4',
            food: '4.4',
            service: '5.0',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4634120.099559119!2d-8.535231978410653!3d55.44952201413343!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x25a3b1142c791a9%3A0xc4f8a0433288257a!2sUnited%20Kingdom!5e0!3m2!1sen!2stw!4v1653870818279!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', '英國', '文化', '人文', '建築', ],
    },
    'c11': {
        id: 'c11',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/netherlands-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/netherlands-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/netherlands-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/netherlands-4.avif',
                style: '',
            },
        ],
        country: `Netherlands`,
        location: `Haarlem, Amsterdam, Giethoorn`,
        describe: `Netherlands is a country located in Northwestern Europe. “Netherlands” literally means "lower countries" in reference to its low elevation and flat topography, with only about 50% of its land exceeding 1 m above sea level. It is the world's second-largest exporter of food and agricultural products by value, owing to its fertile soil, mild climate, intensive agriculture, and inventiveness.`,
        rate: '4.7',
        price: 16500,
        isFavorite: false,
        reviews: {
            transportation: '4.5',
            hotel: '4.6',
            food: '5.0',
            service: '4.7',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1251774.6941224518!2d4.1584338677845665!3d52.20936537380276!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c609c3db87e4bb%3A0xb3a175ceffbd0a9f!2sNetherlands!5e0!3m2!1sen!2stw!4v1653875238379!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', '荷蘭', '文化', '人文', '建築', ],
    },
    'c12': {
        id: 'c12',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/czechRepublic-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/czechRepublic-2.avif',
                style: 'filter: brightness(120%) contrast(80%);',
            },
            {
                src: './asset/image/card/europe/czechRepublic-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/czechRepublic-4.avif',
                style: '',
            },
        ],
        country: `Czech Republic`,
        location: `Český Krumlov, Prague`,
        describe: `Czech Republic, also called Czechia, is a landlocked country in Central Europe. Historically known as Bohemia. The Czech Republic is a developed country with an advanced, high-income social market economy, and boasts 16 UNESCO World Heritage Sites, 3 of them are transnational. Prague is the fifth most visited city in Europe.`,
        rate: '4.6',
        price: 15900,
        isFavorite: false,
        reviews: {
            transportation: '4.4',
            hotel: '5.0',
            food: '4.6',
            service: '4.4',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1318529.008152896!2d14.353950041590123!3d49.800130452815125!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470b948fd7dd8243%3A0xf8661c75d3db586f!2sCzechia!5e0!3m2!1sen!2stw!4v1653833367769!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', '捷克', '文化', '人文', '建築', ],
    },
    'c13': {
        id: 'c13',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/greece-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/greece-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/greece-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/greece-4.avif',
                style: '',
            },
        ],
        country: `Greece`,
        location: `Antiparos, Athens, Zakynthos, Oia`,
        describe: `Greece is located at the crossroads of Europe, Asia, and Africa, and has the longest coastline on the Mediterranean Basin, featuring thousands of islands. Greece is considered the cradle of Western civilization, being the birthplace of democracy, Western philosophy, Western literature, historiography, political science, major scientific and mathematical principles, theatre and the Olympic Games.`,
        rate: '4.5',
        price: 14500,
        isFavorite: false,
        reviews: {
            transportation: '4.2',
            hotel: '4.5',
            food: '4.3',
            service: '5.0',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3193989.8624952286!2d21.667546042441764!3d38.57683735516789!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x135b4ac711716c63%3A0x363a1775dc9a2d1d!2sGreece!5e0!3m2!1sen!2stw!4v1653834023890!5m2!1sen!2stw`,
        tag: [ 'nature', 'culture', 'building', '希臘', '自然', '文化', '人文', '建築', ],
    },
    'c14': {
        id: 'c14',
        continent: 'europe',
        imgSetting: [
            {
                src: './asset/image/card/europe/romania-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/romania-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/romania-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/europe/romania-4.avif',
                style: '',
            },
        ],
        country: `Romania`,
        location: `Ciucaș Peak, Sinaia, Bran`,
        describe: `Romania is a country located at the crossroads of Central, Eastern, and Southeastern Europe. Tourism is a significant contributor to the Romanian economy. Castles, fortifications, or strongholds as well as preserved medieval cities or towns attract a large number of tourists. Rural tourism, focusing on folklore and traditions, has become an important alternative.`,
        rate: '4.6',
        price: 16500,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.3',
            food: '4.6',
            service: '4.5',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2841985.012579998!2d22.777433968062073!3d45.92378458496676!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40b1ff26958976c3%3A0x84ef4f92a804b194!2sRomania!5e0!3m2!1sen!2stw!4v1653875629542!5m2!1sen!2stw`,
        tag: [ 'nature', 'culture', 'building', '羅馬尼亞', '自然', '文化', '人文', '建築', ],
    },
    'c15': {
        id: 'c15',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/japanEast-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/japanEast-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/japanEast-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/japanEast-4.avif',
                style: '',
            },
        ],
        country: `Japan - East`,
        location: `Gunma, Shizuoka, Tokyo, Hokkaido`,
        describe: `Japan is an island country in East Asia, and is a part of the Ring of Fire. A global leader in the automotive, robotics and electronics industries, Japan has made significant contributions to science and technology. The culture of Japan is well known around the world, including its art, cuisine, music, and popular culture, which encompasses prominent comic, animation and video game industries.`,
        rate: '5.0',
        price: 12800,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '5.0',
            food: '5.0',
            service: '5.0',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11982355.775481055!2d135.35990049809823!3d42.84371243721172!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34674e0fd77f192f%3A0xf54275d47c665244!2sJapan!5e0!3m2!1sen!2stw!4v1653870908341!5m2!1sen!2stw`,
        tag: [ 'nature', 'culture', 'building', '日本', '自然', '文化', '人文', '建築', ],
    },
    'c16': {
        id: 'c16',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/japanWest-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/japanWest-2.avif',
                style: 'filter: brightness(120%) contrast(80%);',
            },
            {
                src: './asset/image/card/asia/japanWest-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/japanWest-4.avif',
                style: '',
            },
        ],
        country: `Japan - West`,
        location: `Tokyo, Kyoto, Osaka, Itsukushima`,
        describe: `Japan is an island country in East Asia, and is a part of the Ring of Fire. A global leader in the automotive, robotics and electronics industries, Japan has made significant contributions to science and technology. The culture of Japan is well known around the world, including its art, cuisine, music, and popular culture, which encompasses prominent comic, animation and video game industries.`,
        rate: '4.8',
        price: 11800,
        isFavorite: false,
        reviews: {
            transportation: '4.8',
            hotel: '4.7',
            food: '5.0',
            service: '4.7',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11982355.775481055!2d135.35990049809823!3d42.84371243721172!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34674e0fd77f192f%3A0xf54275d47c665244!2sJapan!5e0!3m2!1sen!2stw!4v1653870908341!5m2!1sen!2stw`,
        tag: [ 'nature', 'culture', 'building', '日本', '自然', '文化', '人文', '建築', ],
    },
    'c17': {
        id: 'c17',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/southKorea-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/southKorea-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/southKorea-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/southKorea-4.avif',
                style: '',
            },
        ],
        country: `South Korea`,
        location: `Seoul`,
        describe: `South Korea is a country in East Asia, constituting the southern part of the Korean Peninsula and sharing a land border with North Korea. Its citizens enjoy one of the world's fastest Internet connection speeds. Since the 21st century, South Korea has been renowned for its globally influential pop culture, particularly in music (K-pop), TV dramas and cinema, a phenomenon referred to as the Korean Wave.`,
        rate: '4.6',
        price: 12500,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.6',
            food: '4.5',
            service: '4.3',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3313549.9076670893!2d125.63011886114498!3d35.80266851494636!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x356455ebcb11ba9b%3A0x91249b00ba88db4b!2sSouth%20Korea!5e0!3m2!1sen!2stw!4v1653871345140!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', 'food', '南韓', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c18': {
        id: 'c18',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/china-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/china-2.avif',
                style: 'filter: brightness(120%);',
            },
            {
                src: './asset/image/card/asia/china-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/china-4.avif',
                style: '',
            },
        ],
        country: `China`,
        location: `Jiangsu, Zhangjiajie, Xi'an`,
        describe: `China is a country in East Asia. It is the world's most populous country. China emerged as one of the world's first civilizations in the fertile basin of the Yellow River in the North China Plain. China is the world's largest economy by GDP at purchasing power parity. The country is one of the fastest growing major economies and is the world's largest manufacturer and exporter.`,
        rate: '4.6',
        price: 10800,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.2',
            food: '4.6',
            service: '4.6',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26950635.25983771!2d86.07893691800834!3d34.4553850891689!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31508e64e5c642c1%3A0x951daa7c349f366f!2sChina!5e0!3m2!1sen!2stw!4v1653871453321!5m2!1sen!2stw`,
        tag: [ 'nature', 'culture', 'building', 'food', '中國', '自然', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c19': {
        id: 'c19',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/thailand-1.avif',
                style: 'filter: brightness(110%);',
            },
            {
                src: './asset/image/card/asia/thailand-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/thailand-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/thailand-4.avif',
                style: 'filter: brightness(120%);',
            },
        ],
        country: `Thailand`,
        location: `Bangkok, Chiang Mai`,
        describe: `Thailand is a country in Southeast Asia, is classified as a newly industrialised economy, with manufacturing, agriculture, and tourism as leading sectors. Thai culture and traditions incorporate a great deal of influence from India, China, Cambodia, and the rest of Southeast Asia. Thai cuisine is one of the most popular in the world.`,
        rate: '4.2',
        price: 11000,
        isFavorite: false,
        reviews: {
            transportation: '3.6',
            hotel: '4.2',
            food: '4.9',
            service: '4.1',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7961366.751750858!2d96.99460933379245!3d13.011066597376624!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x304d8df747424db1%3A0x9ed72c880757e802!2sThailand!5e0!3m2!1sen!2stw!4v1653872690055!5m2!1sen!2stw`,
        tag: [ 'nature', 'animal', 'culture', 'building', 'food', '泰國', '自然', '動物', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c20': {
        id: 'c20',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/india-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/india-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/india-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/india-4.avif',
                style: '',
            },
        ],
        country: `India`,
        location: `Taj Mahal, Nileshwaram, Jaipur, Mapusa`,
        describe: `India is a country in South Asia. It is the second-most populous country, and second only to Africa in human genetic diversity. India is a pluralistic, multilingual and multi-ethnic society, and has become a fast-growing major economy and a hub for information technology services. Indian movies, music, and spiritual teachings play an increasing role in global culture.`,
        rate: '4.0',
        price: 10500,
        isFavorite: false,
        reviews: {
            transportation: '4.5',
            hotel: '4.0',
            food: '4.0',
            service: '3.5',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15359742.591862183!2d63.854590589922495!3d19.96914496960551!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30635ff06b92b791%3A0xd78c4fa1854213a6!2sIndia!5e0!3m2!1sen!2stw!4v1653872762749!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', 'food', '印度', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c21': {
        id: 'c21',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/singapore-1.avif',
                style: 'filter: brightness(120%);',
            },
            {
                src: './asset/image/card/asia/singapore-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/singapore-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/singapore-4.avif',
                style: '',
            },
        ],
        country: `Singapore`,
        location: `Singapore`,
        describe: `Singapore is a sovereign island city-state in maritime Southeast Asia. It has the third highest population density in the world. With a multicultural population and recognising the need to respect cultural identities of the major ethnic groups within the nation, Singapore has four official languages; English, Malay, Mandarin, and Tamil. It is a major financial, maritime shipping and aviation hub.`,
        rate: '4.7',
        price: 12200,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.5',
            food: '4.6',
            service: '4.7',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d255281.1903627398!2d103.70416526649274!3d1.3143393783889257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da11238a8b9375%3A0x887869cf52abf5c4!2sSingapore!5e0!3m2!1sen!2stw!4v1653872876714!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', 'food', '新加坡', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c22': {
        id: 'c22',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/indonesia-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/indonesia-2.avif',
                style: 'filter: brightness(120%);',
            },
            {
                src: './asset/image/card/asia/indonesia-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/indonesia-4.avif',
                style: '',
            },
        ],
        country: `Indonesia`,
        location: `Bali`,
        describe: `Indonesia is a country between the Indian and Pacific oceans, it consists of over 17,000 islands, is the world's largest island country. Indonesia's size, tropical climate, and archipelagic geography support one of the world's highest levels of biodiversity and is among the 17 megadiverse countries identified by Conservation International. Its flora and fauna is a mixture of Asian and Australasian species.`,
        rate: '5.0',
        price: 12800,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '5.0',
            food: '5.0',
            service: '5.0',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d16328041.13317804!2d108.84972636443905!3d-2.3932706563147135!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2c4c07d7496404b7%3A0xe37b4de71badf485!2sIndonesia!5e0!3m2!1sen!2stw!4v1653875313814!5m2!1sen!2stw`,
        tag: [ 'nature', 'building', 'culture', 'food', '印尼', '自然', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c23': {
        id: 'c23',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/vietnam-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/vietnam-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/vietnam-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/vietnam-4.avif',
                style: '',
            },
        ],
        country: `Vietnam`,
        location: `Ho Chi Minh City, Hội An`,
        describe: `Vietnam is a country in Southeast Asia. It went through prolonged warfare through the 20th century. It is nevertheless one of the fastest growing economies of the 21st century. Tourism is an important element of economic activity in the nation, it is one of the fastest growing tourist destinations in the world. Vietnam is home to eight UNESCO World Heritage Sites.`,
        rate: '4.0',
        price: 10500,
        isFavorite: false,
        reviews: {
            transportation: '4.0',
            hotel: '3.5',
            food: '4.2',
            service: '4.3',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7864040.707305019!2d101.41510489415941!3d15.758351946110103!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31157a4d736a1e5f%3A0xb03bb0c9e2fe62be!2sVietnam!5e0!3m2!1sen!2stw!4v1653876007984!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', 'food', '越南', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c24': {
        id: 'c24',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/maldives-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/maldives-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/maldives-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/maldives-4.avif',
                style: '',
            },
        ],
        country: `Maldives`,
        location: `Kendhoo, Noonu Atoll, Veligandu, Kihavah Huravalhi`,
        describe: `Maldives is an archipelagic country located in the Indian subcontinent of Southern Asia, situated in the Indian Ocean. Maldives is one of the world's most geographically dispersed sovereign states and the world's lowest-lying country. Fishing has historically been the dominant economic activity, and remains the largest sector by far, followed by the rapidly growing tourism industry.`,
        rate: '5.0',
        price: 13800,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '5.0',
            food: '5.0',
            service: '5.0',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4079536.63412503!2d70.99627367857116!3d3.115206906254265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x24b599bfaafb7bbd%3A0x414509e181956289!2sMaldives!5e0!3m2!1sen!2stw!4v1653872941751!5m2!1sen!2stw`,
        tag: [ 'nature', 'animal', '馬爾地夫', '自然', '動物', ],
    },
    'c25': {
        id: 'c25',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/russia-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/russia-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/russia-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/russia-4.avif',
                style: '',
            },
        ],
        country: `Russia`,
        location: `Moscow`,
        describe: `Russia is a transcontinental country spanning Eastern Europe and Northern Asia. It is the largest country in the world by area. Russia's extensive mineral and energy resources are the world's largest, and it is among the leading producers of oil and natural gas globally. Russia is also home of 30 UNESCO World Heritage Sites.`,
        rate: '4.5',
        price: 13500,
        isFavorite: false,
        reviews: {
            transportation: '4.5',
            hotel: '4.4',
            food: '4.1',
            service: '5.0',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d42182294.13208505!2d68.81876150090424!3d49.812332541663395!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x453c569a896724fb%3A0x1409fdf86611f613!2sRussia!5e0!3m2!1sen!2stw!4v1653873206546!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', '俄羅斯', '文化', '人文', '建築', ],
    },
    'c26': {
        id: 'c26',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/turkey-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/turkey-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/turkey-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/turkey-4.avif',
                style: 'filter: brightness(110%);',
            },
        ],
        country: `Turkey`,
        location: `Göreme, Istanbul`,
        describe: `Turkey is a transcontinental country located mainly on Anatolia in Western Asia. Turkey is a regional power and a newly industrialized country, with a geopolitically strategic location. It has a rich cultural legacy shaped by centuries of history and the influence of the various peoples that have inhabited its territory over several millennia; it is home to 19 UNESCO World Heritage Sites.`,
        rate: '4.4',
        price: 15300,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.4',
            food: '4.1',
            service: '4.1',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6349270.925549026!2d30.68855032912296!3d39.01006782807502!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14b0155c964f2671%3A0x40d9dbd42a625f2a!2sTurkey!5e0!3m2!1sen!2stw!4v1653873110559!5m2!1sen!2stw`,
        tag: [ 'nature', 'culture', 'building', '土耳其', '自然', '文化', '人文', '建築', ],
    },
    'c27': {
        id: 'c27',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/jordan-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/jordan-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/jordan-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/jordan-4.avif',
                style: '',
            },
        ],
        country: `Jordan`,
        location: `Petra`,
        describe: `Jordan is a country in Western Asia. It is situated at the crossroads of Asia, Africa and Europe. The tourism sector is considered a cornerstone of the economy and is a large source of employment and economic growth. According to the Ministry of Tourism and Antiquities, Jordan is home to around 100,000 archaeological and tourist sites. Some very well preserved historical cities include Petra and Jerash.`,
        rate: '4.3',
        price: 16200,
        isFavorite: false,
        reviews: {
            transportation: '4.3',
            hotel: '5.0',
            food: '3.8',
            service: '4.1',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3491792.9357071524!2d34.88088501204505!3d31.277366521430444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15006f476664de99%3A0x8d285b0751264e99!2sJordan!5e0!3m2!1sen!2stw!4v1653875435820!5m2!1sen!2stw`,
        tag: [ 'nature', 'culture', 'building', 'food', '約旦', '自然', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c28': {
        id: 'c28',
        continent: 'asia',
        imgSetting: [
            {
                src: './asset/image/card/asia/israel-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/israel-2.avif',
                style: 'filter: contrast(110%);',
            },
            {
                src: './asset/image/card/asia/israel-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/asia/israel-4.avif',
                style: '',
            },
        ],
        country: `Israel`,
        location: `Jerusalem`,
        describe: `Israel is a country in Western Asia. It defines itself as a Jewish and democratic state. Israel comprises a major part of the Holy Land, a region of significant importance to Judaism, Christianity and Islam. Tourism, especially religious tourism, is an important industry in Israel, with the country's archaeological, other historical and biblical sites, and unique geography also drawing tourists.`,
        rate: '4.6',
        price: 15400,
        isFavorite: false,
        reviews: {
            transportation: '4.6',
            hotel: '5.0',
            food: '4.2',
            service: '4.6',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1743553.36443287!2d33.96067986702553!3d31.40371922396866!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1500492432a7c98b%3A0x6a6b422013352cba!2sIsrael!5e0!3m2!1sen!2stw!4v1653875694005!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', '', '文化', '人文', '建築', ],
    },
    'c29': {
        id: 'c29',
        continent: 'northamerica',
        imgSetting: [
            {
                src: './asset/image/card/north-america/unitedStatesNature-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/north-america/unitedStatesNature-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/north-america/unitedStatesNature-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/north-america/unitedStatesNature-4.avif',
                style: '',
            },
        ],
        country: `United States - Nature`,
        location: `Lower Antelope Canyon, Albuquerque, Big Sur, Alabama Hills`,
        describe: `United States is a country primarily located in North America. Considered a melting pot of cultures and ethnicities. It ranks high in international measures of economic freedom, quality of life, education, and human rights. United States is the foremost military power in the world and a leading political, cultural, and scientific force. It is the world's largest by GDP at market exchange rates.`,
        rate: '4.8',
        price: 17800,
        isFavorite: false,
        reviews: {
            transportation: '4.6',
            hotel: '4.8',
            food: '5.0',
            service: '4.8',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13004082.928417291!2d-104.65713107818928!3d37.275578278180674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited%20States!5e0!3m2!1sen!2stw!4v1653874606933!5m2!1sen!2stw`,
        tag: [ 'nature', '美國', '自然', ],
    },
    'c30': {
        id: 'c30',
        continent: 'northamerica',
        imgSetting: [
            {
                src: './asset/image/card/north-america/unitedStatesCulture-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/north-america/unitedStatesCulture-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/north-america/unitedStatesCulture-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/north-america/unitedStatesCulture-4.avif',
                style: '',
            },
        ],
        country: `United States - Culture`,
        location: `New York`,
        describe: `United States is a country primarily located in North America. Considered a melting pot of cultures and ethnicities. It ranks high in international measures of economic freedom, quality of life, education, and human rights. United States is the foremost military power in the world and a leading political, cultural, and scientific force. It is the world's largest by GDP at market exchange rates.`,
        rate: '4.6',
        price: 14200,
        isFavorite: false,
        reviews: {
            transportation: '4.2',
            hotel: '4.6',
            food: '5.0',
            service: '4.6',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13004082.928417291!2d-104.65713107818928!3d37.275578278180674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited%20States!5e0!3m2!1sen!2stw!4v1653874606933!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', 'food', '美國', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c31': {
        id: 'c31',
        continent: 'northamerica',
        imgSetting: [
            {
                src: './asset/image/card/north-america/canada-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/north-america/canada-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/north-america/canada-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/north-america/canada-4.avif',
                style: '',
            },
        ],
        country: `Canada`,
        location: `Ontario, Niagara Falls`,
        describe: `Canada is a country in North America. It is one of the world's most ethnically diverse and multicultural nations. The physical geography of Canada is widely varied, it is geologically active, having many earthquakes and potentially active volcanoes. Its advanced economy is relying chiefly upon its abundant natural resources and well-developed international trade networks.`,
        rate: '4.9',
        price: 15200,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.9',
            food: '4.9',
            service: '4.8',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d18876468.200307723!2d-113.72221585646199!3d54.72270517403909!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b0d03d337cc6ad9%3A0x9968b72aa2438fa5!2sCanada!5e0!3m2!1sen!2stw!4v1653874709086!5m2!1sen!2stw`,
        tag: [ 'nature', 'animal', '加拿大', '自然', '動物', ],
    },
    'c32': {
        id: 'c32',
        continent: 'northamerica',
        imgSetting: [
            {
                src: './asset/image/card/north-america/hawaii-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/north-america/hawaii-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/north-america/hawaii-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/north-america/hawaii-4.avif',
                style: '',
            },
        ],
        country: `Hawaii`,
        location: `Waialua, O‘ahu`,
        describe: `Hawaii is a state in the Western United States, located in the Pacific Ocean. Historically dominated by a plantation economy. The state attracts tourists, surfers, and scientists from around the world with its diverse natural scenery, warm tropical climate, abundance of public beaches, oceanic surroundings, active volcanoes, and clear skies on the Big Island.`,
        rate: '4.8',
        price: 13900,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.8',
            food: '4.7',
            service: '4.7',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1913933.4179216716!2d-158.6262063686437!3d20.45898184295111!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7bffdb064f79e005%3A0x4b7782d274cc8628!2sHawaii%2C%20USA!5e0!3m2!1sen!2stw!4v1653875886237!5m2!1sen!2stw`,
        tag: [ 'nature', 'animal', 'food',  '夏威夷', '自然', '動物', '食物', '美食', ],
    },
    'c33': {
        id: 'c33',
        continent: 'southamerica',
        imgSetting: [
            {
                src: './asset/image/card/south-america/brazil-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/south-america/brazil-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/south-america/brazil-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/south-america/brazil-4.avif',
                style: '',
            },
        ],
        country: `Brazil`,
        location: `Rio de Janeiro`,
        describe: `Brazil is the largest country in South America. It is one of the most multicultural and ethnically diverse nations. Its Amazon basin includes a vast tropical forest, home to diverse wildlife, a variety of ecological systems, and extensive natural resources spanning numerous protected habitats. This unique environmental heritage makes Brazil one of 17 megadiverse countries.`,
        rate: '4.8',
        price: 14000,
        isFavorite: false,
        reviews: {
            transportation: '4.8',
            hotel: '4.8',
            food: '4.6',
            service: '5.0',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31759704.64315484!2d-69.64649036266651!3d-13.662806394470584!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9c59c7ebcc28cf%3A0x295a1506f2293e63!2sBrazil!5e0!3m2!1sen!2stw!4v1653874785858!5m2!1sen!2stw`,
        tag: [ 'nature', 'animal', 'culture', 'building', 'food', '巴西', '自然', '動物', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c34': {
        id: 'c34',
        continent: 'southamerica',
        imgSetting: [
            {
                src: './asset/image/card/south-america/chile-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/south-america/chile-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/south-america/chile-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/south-america/chile-4.avif',
                style: '',
            },
        ],
        country: `Chile`,
        location: `Easter Island, San Pedro de Atacama, Patagonia`,
        describe: `Chile is a country in the western part of South America, and is located along a highly seismic and volcanic zone, part of the Pacific Ring of Fire. It is the southernmost country in the world, and the closest to Antarctica. Chile within its borders hosts at least ten major climatic subtypes. The flora and fauna of Chile are characterized by a high degree of endemism, due to its particular geography.`,
        rate: '4.8',
        price: 15500,
        isFavorite: false,
        reviews: {
            transportation: '4.8',
            hotel: '4.8',
            food: '5.0',
            service: '4.6',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26637893.2539379!2d-106.20571966769492!3d-35.412786241562905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662c5410425af2f%3A0x505e1131102b91d!2sChile!5e0!3m2!1sen!2stw!4v1653874922528!5m2!1sen!2stw`,
        tag: [ 'nature', 'culture', 'building', '智利', '自然', '文化', '人文', '建築', ],
    },
    'c35': {
        id: 'c35',
        continent: 'southamerica',
        imgSetting: [
            {
                src: './asset/image/card/south-america/mexico-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/south-america/mexico-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/south-america/mexico-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/south-america/mexico-4.avif',
                style: '',
            },
        ],
        country: `Mexico`,
        location: `Chichén Itzá`,
        describe: `Mexico is a country in the southern portion of North America. Is identified as one of the world's six cradles of civilization. Mexico ranks first in the Americas for the number of UNESCO World Heritage Sites. Mexico's rich cultural and biological heritage, as well as varied climate and geography, makes it a major tourist destination.`,
        rate: '4.4',
        price: 14200,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.2',
            food: '4.0',
            service: '4.4',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15007910.294888817!2d-111.64025257816783!3d23.314285701531734!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x84043a3b88685353%3A0xed64b4be6b099811!2sMexico!5e0!3m2!1sen!2stw!4v1653875837967!5m2!1sen!2stw`,
        tag: [ 'culture', 'building', 'food', '墨西哥', '文化', '人文', '建築', '食物', '美食', ],
    },
    'c36': {
        id: 'c36',
        continent: 'africa',
        imgSetting: [
            {
                src: './asset/image/card/africa/egypt-1.avif',
                style: 'filter: brightness(110%);',
            },
            {
                src: './asset/image/card/africa/egypt-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/africa/egypt-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/africa/egypt-4.avif',
                style: '',
            },
        ],
        country: `Egypt`,
        location: `Louxor, Abu Simbel, Cairo`,
        describe: `Egypt is a transcontinental country. Considered a cradle of civilisation, Ancient Egypt saw some of the earliest developments of writing, agriculture and organised religion. Iconic monuments such as the Giza Necropolis and it’s Great Sphinx, as well the ruins of Memphis, Thebes, Karnak, and the Valley of the Kings, reflect this legacy and remain a significant focus of scientific and popular interest.`,
        rate: '4.7',
        price: 16000,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.5',
            food: '4.6',
            service: '4.7',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7290565.964616149!2d26.384040060386805!3d26.844828983225547!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14368976c35c36e9%3A0x2c45a00925c4c444!2sEgypt!5e0!3m2!1sen!2stw!4v1653873275656!5m2!1sen!2stw`,
        tag: [ 'nature', 'animal', 'culture', 'building', '埃及', '自然', '動物', '文化', '人文', '建築', ],
    },
    'c37': {
        id: 'c37',
        continent: 'africa',
        imgSetting: [
            {
                src: './asset/image/card/africa/kenya-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/africa/kenya-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/africa/kenya-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/africa/kenya-4.avif',
                style: '',
            },
        ],
        country: `Kenya`,
        location: `Amboseli, Maasai Mara National Reserve`,
        describe: `Kenya is a country in Eastern Africa. Its geography, climate and population vary widely. Kenya has considerable land area devoted to wildlife habitats, a significant population of wild animals, reptiles, and birds can be found in the national parks and game reserves in the country. The annual animal migration occurs between June and September, with millions of animals taking part.`,
        rate: '4.3',
        price: 18000,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '3.9',
            food: '4.3',
            service: '4.0',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4085558.0417771228!2d35.66430293831735!3d0.15994068492205113!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182780d08350900f%3A0x403b0eb0a1976dd9!2sKenya!5e0!3m2!1sen!2stw!4v1653873338954!5m2!1sen!2stw`,
        tag: [ 'nature', 'animal', '肯亞', '自然', '動物', ],
    },
    'c38': {
        id: 'c38',
        continent: 'africa',
        imgSetting: [
            {
                src: './asset/image/card/africa/southAfrica-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/africa/southAfrica-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/africa/southAfrica-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/africa/southAfrica-4.avif',
                style: '',
            },
        ],
        country: `South Africa`,
        location: `Kruger National Park, Johannesburg`,
        describe: `South Africa is the southernmost country in Africa. It is a biodiversity hotspot, with a diversity of unique biomes and plant and animal life and is among the 17 megadiverse countries identified by Conservation International. Ecotourism in South Africa has become more prevalent in recent years, as a possible method of maintaining and improving biodiversity.`,
        rate: '4.3',
        price: 17300,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '3.9',
            food: '4.0',
            service: '4.3',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7322660.661788738!2d21.5968336335546!3d-26.34210602337809!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1c34a689d9ee1251%3A0xe85d630c1fa4e8a0!2sSouth%20Africa!5e0!3m2!1sen!2stw!4v1653875544455!5m2!1sen!2stw`,
        tag: [ 'nature', 'animal', 'culture', 'building', '南非', '自然', '動物', '文化', '人文', '建築', ],
    },
    'c39': {
        id: 'c39',
        continent: 'oceania',
        imgSetting: [
            {
                src: './asset/image/card/oceania/australia-1.avif',
                style: 'filter: brightness(120%);',
            },
            {
                src: './asset/image/card/oceania/australia-2.avif',
                style: 'filter: brightness(120%);',
            },
            {
                src: './asset/image/card/oceania/australia-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/oceania/australia-4.avif',
                style: '',
            },
        ],
        country: `Australia`,
        location: `Uluru, Barossa Valley, Gold Coast`,
        describe: `Australia is the oldest, flattest, and driest inhabited continent, with the least fertile soils. It is a megadiverse country, and its size gives it a wide variety of landscapes and climates. Australia ranks highly in quality of life, with all its major cities faring exceptionally in global comparative livability surveys.`,
        rate: '4.8',
        price: 16800,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '4.7',
            food: '4.8',
            service: '4.7',
        },
        map: `https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14685935.386031656!2d130.9121284593738!3d-26.01937036921441!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2b2bfd076787c5df%3A0x538267a1955b1352!2sAustralia!5e0!3m2!1sen!2stw!4v1653828476578!5m2!1sen!2stw`,
        tag: [ 'nature', 'animal', 'food', '澳洲', '自然', '動物', '食物', '美食', ],
    },
    'c40': {
        id: 'c40',
        continent: 'oceania',
        imgSetting: [
            {
                src: './asset/image/card/oceania/newZealand-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/oceania/newZealand-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/oceania/newZealand-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/oceania/newZealand-4.avif',
                style: '',
            },
        ],
        country: `New Zealand`,
        location: `Southland, Milford Sound, Matamata, Wellington`,
        describe: `New Zealand is an island country in the southwestern Pacific Ocean. It consists of two main landmasses—the North Island and the South Island, and over 700 smaller islands. The service sector dominates the national economy, followed by the industrial sector, and agriculture. International tourism is also a significant source of revenue.`,
        rate: '4.9',
        price: 17200,
        isFavorite: false,
        reviews: {
            transportation: '4.9',
            hotel: '4.9',
            food: '5.0',
            service: '4.8',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6207953.670660351!2d170.5061902097666!3d-40.55865577178507!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d2c200e17779687%3A0xb1d618e2756a4733!2sNew%20Zealand!5e0!3m2!1sen!2stw!4v1653831395841!5m2!1sen!2stw`,
        tag: [ 'nature', 'animal', 'culture', 'building', '紐西蘭', '自然', '動物', '文化', '人文', '建築', ],
    },
    'c41': {
        id: 'c41',
        continent: 'antarctica',
        imgSetting: [
            {
                src: './asset/image/card/antarctica/antarctica-1.avif',
                style: '',
            },
            {
                src: './asset/image/card/antarctica/antarctica-2.avif',
                style: '',
            },
            {
                src: './asset/image/card/antarctica/antarctica-3.avif',
                style: '',
            },
            {
                src: './asset/image/card/antarctica/antarctica-4.avif',
                style: 'filter: brightness(120%);',
            },
        ],
        country: `Antarctica`,
        location: `Antarctica`,
        describe: `Antarctica is Earth's southernmost continent. About 70% of the world's freshwater reserves are frozen in Antarctica, which if melted would raise global sea levels by almost 60 meters. Antarctica holds the record for the lowest measured temperature on Earth. Native species of animals include mites, nematodes, penguins, seals and tardigrades. Where vegetation occurs, it is mostly in the form of lichen or moss.`,
        rate: '5.0',
        price: 20000,
        isFavorite: false,
        reviews: {
            transportation: '5.0',
            hotel: '5.0',
            food: '5.0',
            service: '5.0',
        },
        map: `https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23243278.44060305!2d0!3d-69.17165706722953!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xa4b9967b3390754b%3A0x6e52be1f740f2075!2sAntarctica!5e0!3m2!1sen!2stw!4v1653874994595!5m2!1sen!2stw`,
        tag: [ 'nature', 'animal', '南極', '自然', '動物', ],
    },
};

const _favorites = {
    '127.0.0.1': _defaultFavorites(),
};

function _defaultFavorites() {
    return [
        'c00', 'c03', 'c05', 'c15', 'c33',
        'c36', 'c40', 'c41',
    ];
}

function _userFavorites(ip) {
    if (!_favorites.hasOwnProperty(ip)) {
        _favorites[ip] = _defaultFavorites();
    }
    return _favorites[ip];
}

module.exports = {
    init(ip) {
        delete _favorites[ip];
    },
    products(ip) {
        const products = copy(_PRODUCTS);
        _userFavorites(ip)
            .forEach((productID) => {
                products[productID].isFavorite = true;
            });
        return Object.values(products);
    },
    product(ip, id) {
        const product = copy(_PRODUCTS[id]);
        const userFavorite = _userFavorites(ip);
        product.isFavorite = userFavorite.includes(product.id);
        return product;
    },
    updateFavorite(ip, id, action) {
        const userFavorite = _userFavorites(ip);
        const isExist = userFavorite.includes(id);
        if (action === 'add' && !isExist) {
               userFavorite.push(id);
        }
        if (action === 'remove' && isExist) {
            const index = userFavorite.indexOf(id);
            userFavorite.splice(index, 1);
        }
    },
    resetImage(product, width, quality) {
        product.imgSetting
            .forEach((setting) => {
                if (width) {
                    setting.src = setting.src.replace(/w=\d+/g, `w=${width}`);
                }
                if (quality) {
                    setting.src = setting.src.replace(/q=\d+/g, `q=${quality}`);
                }
            });
    }
};
