const { copy, randomIndex } = require('../utils/helpers');

const _SEATS = [
    '1A', '1B', '1C', '1D',
    '2A', '2B', '2C', '2D',
    '3A', '3B', '3C', '3D',
    '4A', '4B', '4C', '4D',
    '5A', '5B', '5C', '5D',
];

const _FLIGHTS = {
    't00': {
        id: 't00',
        cardID: 'c00',
        departure: {
            time: `09:50`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `08:30`,
            place: `Reykjavík`,
            airport: `KEF`,
        },
        elapsedTime: `21h20m`,
    },
    't01': {
        id: 't01',
        cardID: 'c01',
        departure: {
            time: `16:10`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `08:05`,
            place: `Oslo`,
            airport: `OSL`,
        },
        elapsedTime: `21h55m`,
    },
    't02': {
        id: 't02',
        cardID: 'c02',
        departure: {
            time: `14:05`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `06:20`,
            place: `Zürich`,
            airport: `ZRH`,
        },
        elapsedTime: `22h15m`,
    },
    't03': {
        id: 't03',
        cardID: 'c03',
        departure: {
            time: `14:05`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `05:35`,
            place: `Vienna`,
            airport: `VIE`,
        },
        elapsedTime: `21h30m`,
    },
    't04': {
        id: 't04',
        cardID: 'c04',
        departure: {
            time: `16:10`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `08:25`,
            place: `Berlin`,
            airport: `BER`,
        },
        elapsedTime: `22h15m`,
    },
    't05': {
        id: 't05',
        cardID: 'c05',
        departure: {
            time: `16:10`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `09:15`,
            place: `Brussels`,
            airport: `BRU`,
        },
        elapsedTime: `23h05m`,
    },
    't06': {
        id: 't06',
        cardID: 'c06',
        departure: {
            time: `23:40`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `08:15`,
            place: `Paris`,
            airport: `CDG`,
        },
        elapsedTime: `14h35m`,
    },
    't07': {
        id: 't07',
        cardID: 'c07',
        departure: {
            time: `14:05`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `08:35`,
            place: `Barcelona`,
            airport: `BCN`,
        },
        elapsedTime: `24h30m`,
    },
    't08': {
        id: 't08',
        cardID: 'c08',
        departure: {
            time: `15:55`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `08:05`,
            place: `Rome`,
            airport: `FCO`,
        },
        elapsedTime: `22h10m`,
    },
    't09': {
        id: 't09',
        cardID: 'c09',
        departure: {
            time: `15:55`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `08:05`,
            place: `Rome`,
            airport: `FCO`,
        },
        elapsedTime: `22h10m`,
    },
    't10': {
        id: 't10',
        cardID: 'c10',
        departure: {
            time: `15:55`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `05:55`,
            place: `London`,
            airport: `LHR`,
        },
        elapsedTime: `21h00m`,
    },
    't11': {
        id: 't11',
        cardID: 'c11',
        departure: {
            time: `15:55`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `07:15`,
            place: `Amsterdam`,
            airport: `AMS`,
        },
        elapsedTime: `21h20m`,
    },
    't12': {
        id: 't12',
        cardID: 'c12',
        departure: {
            time: `14:20`,
            place: `Taipei`,
            airport: `TSA`,
        },
        arrival: {
            time: `09:25`,
            place: `Prague`,
            airport: `PRG`,
        },
        elapsedTime: `25h05m`,
    },
    't13': {
        id: 't13',
        cardID: 'c13',
        departure: {
            time: `16:10`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `09:05`,
            place: `Athens`,
            airport: `ATH`,
        },
        elapsedTime: `21h55m`,
    },
    't14': {
        id: 't14',
        cardID: 'c14',
        departure: {
            time: `14:05`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `09:05`,
            place: `Otopeni`,
            airport: `OTP`,
        },
        elapsedTime: `24h00m`,
    },
    't15': {
        id: 't15',
        cardID: 'c15',
        departure: {
            time: `06:45`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `11:05`,
            place: `Narita`,
            airport: `NRT`,
        },
        elapsedTime: `03h20m`,
    },
    't16': {
        id: 't16',
        cardID: 'c16',
        departure: {
            time: `06:45`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `11:05`,
            place: `Narita`,
            airport: `NRT`,
        },
        elapsedTime: `03h20m`,
    },
    't17': {
        id: 't17',
        cardID: 'c17',
        departure: {
            time: `16:10`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `19:45`,
            place: `Incheon`,
            airport: `ICN`,
        },
        elapsedTime: `02h35m`,
    },
    't18': {
        id: 't18',
        cardID: 'c18',
        departure: {
            time: `11:15`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `13:25`,
            place: `Shanghai`,
            airport: `PVG`,
        },
        elapsedTime: `02h10m`,
    },
    't19': {
        id: 't19',
        cardID: 'c19',
        departure: {
            time: `14:05`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `16:50`,
            place: `Bangkok`,
            airport: `BKK`,
        },
        elapsedTime: `03h45m`,
    },
    't20': {
        id: 't20',
        cardID: 'c20',
        departure: {
            time: `14:05`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `21:55`,
            place: `Mumbai`,
            airport: `BOM`,
        },
        elapsedTime: `10h20m`,
    },
    't21': {
        id: 't21',
        cardID: 'c21',
        departure: {
            time: `15:55`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `20:30`,
            place: `Singapore`,
            airport: `SIN`,
        },
        elapsedTime: `03h20m`,
    },
    't22': {
        id: 't22',
        cardID: 'c22',
        departure: {
            time: `15:55`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `09:50`,
            place: `Bali`,
            airport: `DPS`,
        },
        elapsedTime: `17h55m`,
    },
    't23': {
        id: 't23',
        cardID: 'c23',
        departure: {
            time: `14:10`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `16:45`,
            place: `Hồ Chí Minh`,
            airport: `SGN`,
        },
        elapsedTime: `03h35m`,
    },
    't24': {
        id: 't24',
        cardID: 'c24',
        departure: {
            time: `21:30`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `09:30`,
            place: `Malé`,
            airport: `MLE`,
        },
        elapsedTime: `15h00m`,
    },
    't25': {
        id: 't25',
        cardID: 'c25',
        departure: {
            time: `14:05`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `08:25`,
            place: `Moscow`,
            airport: `SVO`,
        },
        elapsedTime: `23h20m`,
    },
    't26': {
        id: 't26',
        cardID: 'c26',
        departure: {
            time: `16:10`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `05:10`,
            place: `Istanbul`,
            airport: `IST`,
        },
        elapsedTime: `18h00m`,
    },
    't27': {
        id: 't27',
        cardID: 'c27',
        departure: {
            time: `07:20`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `21:45`,
            place: `Amman`,
            airport: `AMM`,
        },
        elapsedTime: `19h25m`,
    },
    't28': {
        id: 't28',
        cardID: 'c28',
        departure: {
            time: `13:50`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `07:45`,
            place: `Tel Aviv`,
            airport: `TLV`,
        },
        elapsedTime: `22h55m`,
    },
    't29': {
        id: 't29',
        cardID: 'c29',
        departure: {
            time: `09:00`,
            place: `Taipei`,
            airport: `TSA`,
        },
        arrival: {
            time: `21:47`,
            place: `Flagstaff`,
            airport: `FLG`,
        },
        elapsedTime: `27h47m`,
    },
    't30': {
        id: 't30',
        cardID: 'c30',
        departure: {
            time: `23:30`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `07:00`,
            place: `New York`,
            airport: `JFK`,
        },
        elapsedTime: `19h30m`,
    },
    't31': {
        id: 't31',
        cardID: 'c31',
        departure: {
            time: `23:30`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `06:33`,
            place: `Toronto`,
            airport: `YYZ`,
        },
        elapsedTime: `19h03m`,
    },
    't32': {
        id: 't32',
        cardID: 'c32',
        departure: {
            time: `14:20`,
            place: `Taipei`,
            airport: `TSA`,
        },
        arrival: {
            time: `09:55`,
            place: `Honolulu`,
            airport: `HNL`,
        },
        elapsedTime: `13h35m`,
    },
    't33': {
        id: 't33',
        cardID: 'c33',
        departure: {
            time: `09:50`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `11:15`,
            place: `Rio de Janeiro`,
            airport: `GIG`,
        },
        elapsedTime: `36h25m`,
    },
    't34': {
        id: 't34',
        cardID: 'c34',
        departure: {
            time: `09:50`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `06:20`,
            place: `Santiago`,
            airport: `SCL`,
        },
        elapsedTime: `32h30m`,
    },
    't35': {
        id: 't35',
        cardID: 'c35',
        departure: {
            time: `09:50`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `20:10`,
            place: `Merida`,
            airport: `MID`,
        },
        elapsedTime: `23h20m`,
    },
    't36': {
        id: 't36',
        cardID: 'c36',
        departure: {
            time: `14:05`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `06:40`,
            place: `Cairo`,
            airport: `CAI`,
        },
        elapsedTime: `22h35m`,
    },
    't37': {
        id: 't37',
        cardID: 'c37',
        departure: {
            time: `10:35`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `20:20`,
            place: `Nairobi`,
            airport: `NBO`,
        },
        elapsedTime: `38h45m`,
    },
    't38': {
        id: 't38',
        cardID: 'c38',
        departure: {
            time: `15:55`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `06:10`,
            place: `Johannesburg`,
            airport: `JNB`,
        },
        elapsedTime: `20h15m`,
    },
    't39': {
        id: 't39',
        cardID: 'c39',
        departure: {
            time: `14:05`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `12:05`,
            place: `Alice Springs`,
            airport: `ASP`,
        },
        elapsedTime: `20h30m`,
    },
    't40': {
        id: 't40',
        cardID: 'c40',
        departure: {
            time: `10:35`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `10:10`,
            place: `Wellington`,
            airport: `WLG`,
        },
        elapsedTime: `19h35m`,
    },
    't41': {
        id: 't41',
        cardID: 'c41',
        departure: {
            time: `23:30`,
            place: `Taoyuan`,
            airport: `TPE`,
        },
        arrival: {
            time: `08:05`,
            place: `Ushuaia`,
            airport: `USH`,
        },
        elapsedTime: `43h35m`,
    },
};

const _flightDateSeats = {
    // '127.0.0.1': {
    //     'ID-timestamp': {
    //         departure: [],
    //         arrival: [],
    //     }
    // }
};

function _randomSeats() {
    const map = {};
    // default every seat is not available
    _SEATS.forEach((seat) => map[seat] = false);
    // random pick 12 seats as available
    new Array(12)
        .fill('')
        .forEach(() => {
            const index = randomIndex(_SEATS.length, (i) => !map[_SEATS[i]]);
            map[_SEATS[index]] = true;
        });
    return map;
}

function _userFlightDateSeats(ip, id, timestamp) {
    if (!_flightDateSeats.hasOwnProperty(ip)) {
        _flightDateSeats[ip] = {};
    }

    const key = `${id}-${timestamp}`;
    if (!_flightDateSeats[ip].hasOwnProperty(key)) {
        _flightDateSeats[ip][key] = {
            departure: _randomSeats(),
            arrival: _randomSeats(),
        };
    }

    return _flightDateSeats[ip][key];
}

module.exports = {
    init(ip) {
        delete _flightDateSeats[ip];
    },
    flightByID(flightID) {
        return copy(_FLIGHTS[flightID]);
    },
    flightByProductID(productID) {
        const rawFlight = Object
            .values(_FLIGHTS)
            .find(({ cardID }) => cardID === productID);
        return copy(rawFlight);
    },
    seats(ip, { id, timestamp }) {
        return _userFlightDateSeats(ip, id, timestamp);
    },
    isAvailableSeat(ip, { id, timestamp, type, seat }) {
        return _userFlightDateSeats(ip, id, timestamp)[type][seat];
    },
    updateSeat(ip, { id, timestamp, type, seat }, toAvailable) {
        const userSeats = _userFlightDateSeats(ip, id, timestamp);
        userSeats[type][seat] = toAvailable;
    },
};

