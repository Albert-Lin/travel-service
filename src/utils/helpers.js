const IP_HEADERS = [
    'Forwarded',
    'Forwarded-For',
    'X-Forwarded',
    'X-Forwarded-For',     // may contain multiple IP addresses in the format: 'client IP, proxy 1 IP, proxy 2 IP' - we use first one
    'X-Client-IP',
    'X-Real-IP',           // Nginx proxy, FastCGI
    'X-Cluster-Client-IP', // Rackspace LB, Riverbed Stingray
    'Proxy-Client-IP',
    'CF-Connecting-IP',    // Cloudflare
    'Fastly-Client-Ip',    // Fastly CDN and Firebase hosting header when forwared to a cloud function
    'True-Client-Ip',      // Akamai and Cloudflare
    'WL-Proxy-Client-IP',
    'HTTP_X_FORWARDED_FOR',
    'HTTP_X_FORWARDED',
    'HTTP_X_CLUSTER_CLIENT_IP',
    'HTTP_CLIENT_IP',
    'HTTP_FORWARDED_FOR',
    'HTTP_FORWARDED',
    'HTTP_VIA',
    'REMOTE_ADDR'
];

module.exports = {
    successResponse(response, data, successCode = 0, status = 200) {
        response
            .status(status)
            .json({
                code: successCode,
                msg: null,
                data
            });
    },
    errorResponse(response, error, errorCode = 500, status = 200) {
        response
            .status(status)
            .json({
                code: errorCode,
                msg: error.message,
                data: null,
            });
    },
    waitFor(ms) {
        return new Promise(resolve => {
            setTimeout(() => resolve(undefined), ms);
        });
    },
    copy(data) {
        return JSON.parse(JSON.stringify(data));
    },
    id(prefix) {
        const nowTimestamp = new Date().getTime();
        const randomNum = Math.floor(Math.random() * 1000);
        return `${prefix}-${nowTimestamp}-${randomNum}`;
    },
    requestIP(request) {
        const headers = request.headers;
        for (const header of IP_HEADERS) {
            const value = headers[header];
            if (value) {
                const parts = value.split(/\s*,\s*/g);
                return parts[0] || null;
            }
        }
        const client = request.connection
            ? request.connection
            : request.socket
                ? request.socket
                : request.info;

        if (client) {
            return client.remoteAddress || null;
        }
        return request.ip || null;
    },
    registerAllMiddlewares(app, middlewares) {
        middlewares.forEach((middleware) => app.use(middleware));
    },
    randomIndex(max, breakCondition) {
        let result = -1;
        while(true) {
            result = Math.floor(Math.random() * max);
            if (breakCondition(result)) {
                break;
            }
        }
        return result;
    }
};
