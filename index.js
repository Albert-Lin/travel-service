const express = require('express');
const cors = require('cors');
const app = express();
const port = process.env.PORT || 80;
const routes = require('./src/routers/index');
const middlewares = require('./src/middlewares/Global');
const { networkInterfaces } = require('os');
const { registerAllMiddlewares } = require('./src/utils/helpers');
const iNetwork = networkInterfaces();

const corsOptions = { origin: '*' };

function _registerRouteByConfig({ method, path, handler }) {
    const prefix = path === '' ? '' : '/api';
    app[method.toLowerCase()](`${prefix}/${ path }`, handler);
}

function _serverUp() {
    Object
        .values(iNetwork)
        .flat()
        .filter(({ family }) => family === 'IPv4')
        .forEach(({ address }) => {
            console.log(`Server Up: ${ address }:${ port }`);
        });
}

app.use(cors(corsOptions));
app.use(express.json());
routes.forEach(_registerRouteByConfig);
registerAllMiddlewares(app, middlewares);
app.disable('etag');
app.listen(port, '0.0.0.0', _serverUp);
